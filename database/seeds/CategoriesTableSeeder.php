<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Category::create([
        'name' => 'Advice',
	    ]);
	    Category::create([
        'name' => 'Announcement',
	    ]);
	    Category::create([
        'name' => 'Press Release',
	    ]);
	    Category::create([
        'name' => 'Information',
	    ]);
    }
}
