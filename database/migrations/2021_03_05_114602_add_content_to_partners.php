<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContentToPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->string('second_title')->nullable()->after('body');
            $table->string('second_excerpt')->nullable()->after('second_title');
            $table->text('second_content')->nullable()->after('second_excerpt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('second_title');
            $table->dropColumn('second_excerpt');
            $table->dropColumn('second_content');
        });
    }
}
