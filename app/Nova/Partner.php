<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Trix;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Partner extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Partner';
    public static $with = ['media'];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Title')->sortable(),
            Image::make('Photo')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('partners');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('partners', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('partners', 'thumbnail');
            })->deletable(false),
            Image::make('Diagram'),
            Text::make('Location')->sortable(),
            Text::make('Latitude')->sortable()->onlyOnForms(),
            Text::make('Longitude')->sortable()->onlyOnForms(),
            Select::make('Status')->options($statuses)->sortable(),
            Text::make('Link')->onlyOnForms(),
            Trix::make('Body')->withFiles('public'),
            Text::make('Second Title', 'second_title')->onlyOnForms()->nullable(),
            Text::make('Second Excerpt', 'second_excerpt')->onlyOnForms()->nullable(),
            Trix::make('Second Content', 'second_content')->onlyOnForms()->nullable()->withFiles('public'),
            HasMany::make('Leases','leases')->sortable(),
            HasMany::make('Positions','positions')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
