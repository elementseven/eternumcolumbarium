<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Partner extends Model implements HasMedia
{
	use HasMediaTrait;

	protected static function boot()
	{
		parent::boot();
    static::saving(function ($post) {
      $slug = Str::slug($post->location, "-") .'-'. Str::slug($post->title, "-");
      $post->slug = $slug;
    });
	}

	protected $casts = [
		'created_at' => 'datetime',
	];

	public function registerMediaConversions(Media $media = null)
	{
		$this->addMediaConversion('normal')->width(600)->crop('crop-center', 600, 400);
    $this->addMediaConversion('normal-webp')->width(600)->crop('crop-center', 600, 400)->format('webp');
    $this->addMediaConversion('double')->width(1200)->crop('crop-center', 1200, 800);
    $this->addMediaConversion('double-webp')->width(1200)->crop('crop-center', 1200, 800)->format('webp');
    $this->addMediaConversion('thumbnail')->width(400)->crop('crop-center', 400, 400);
	}
	public function registerMediaCollections()
	{
		$this->addMediaCollection('partners')->singleFile();
	}
	public function leases(){
    return $this->hasMany('App\Lease');
  }
  public function positions(){
    return $this->hasMany('App\Position');
  }
}