<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lease extends Model
{

	protected $fillable = [
    'title', 'price', 'tax', 'description', 'partner_id'
  ];

  protected static function boot()
  {
    parent::boot();
    static::saving(function ($lease) {
        $lease->tax = ($lease->price) / 100 * 13.5;
    });
  }


  public function partner(){
    return $this->belongsTo('App\Partner');
  }
  public function positions(){
    return $this->hasMany('App\Position');
  }
}