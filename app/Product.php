<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $fillable = [
        'name','slug', 'excerpt', 'price', 'sale_price', 'description', 'image', 'status'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($product) {
          $product->slug = Str::slug($product->name, "-");
        });
    }
    public function users(){
        return $this->belongsToMany('App\User', 'product_user');
    }
    public function invoices(){
        return $this->belongsToMany('App\Invoice', 'invoice_product')->withPivot('invoice_id','product_id','paid');
    }
    
}
