<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Address;
use App\Position;
use App\Invoice;
use App\Lease;
use App\User;

use Auth;
use Cart;
use Hash;
use Mail;

class ShopController extends Controller
{	

	// Function to create a user
 public function createUser(Request $request){

  $this->validate($request,[
    'first_name' => 'required|string|max:255',
    'last_name' => 'required|string|max:255',
    'email' => 'required|string|email|max:255|unique:users',
    'password' => 'required|string|min:8|confirmed',
    'consent' => 'accepted'
  ]);

  $user = User::create([
    'first_name' => $request->input('first_name'),
    'last_name' => $request->input('last_name'),
    'email' => $request->input('email'),
    'phone' => $request->input('phone'),
    'password' => Hash::make($request->input('password')),
    'role_id' => 2
  ]);
  Auth::login($user);


  \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
  $intent = \Stripe\PaymentIntent::create([
    'amount' => Cart::total(2,'.','') * 100,
    'currency' => 'eur',
    // Verify your integration in this guide by including this parameter
    'metadata' => ['integration_check' => 'accept_a_payment'],
  ]);

    //Send the user login details for their account
  $this->accountCreated($user);

  return $intent;

}

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function storeAddress(Request $request)
  {
  	if($request->input('same') == 1){
      // Validate the form data
      $this->validate($request,[
        'streetAddress' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'postalCode' => 'required|string|max:255',
        'country' => 'required|string|max:255'
      ]);
    }else{
      // Validate the form data
      $this->validate($request,[
        'streetAddress' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'postalCode' => 'required|string|max:255',
        'country' => 'required|string|max:255',
        'shippingstreetAddress' => 'required|string|max:255',
        'shippingcity' => 'required|string|max:255',
        'shippingpostalCode' => 'required|string|max:255',
        'shippingcountry' => 'required|string|max:255'
      ]);
    }

    // Store the user's address(es)
    $billingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), 'billing');

    if($request->input('same') == 1){
      $shippingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), 'shipping');
    }else{
      $shippingAddress = $this->createAddress($request->input('shippingstreetAddress'), $request->input('shippingextendedAddress'),$request->input('shippingcity'), $request->input('shippingpostalCode'), $request->input('shippingcountry'), $request->input('shippingregion'), 'shipping');
    }

    return $billingAddress;
  }



	/**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function payment(Request $request)
	{
    $user = Auth::user();

    $invoice = Invoice::create([
      'transaction_id' => $request->input('paymentId'),
      'price' => Cart::total(2,'.',''),
      'paid' => 1,
      'user_id' => $user->id
    ]);

    foreach(Cart::content() as $cc){
      $position = Position::where('id', $cc->model->id)->first();
      $position->invoice_id = $invoice->id;
      $position->lease_id = $cc->options->lease->id;
      $position->user_id = $user->id;
      $position->save();
    }

  	// Sign the new customer up to a mailing list
  	// if($request->input('mailchimp') == 1){
  	// 	Newsletter::subscribe($user->email, ['FNAME'=>$user->first_name, 'LNAME'=>$user->last_name]);
  	// }

    // Send confirmation emails
    $this->paymentConfirmed($invoice, $user);
    $this->accountCreated($user);
    Cart::destroy();
    return $invoice;
  }

  public function getIntent(){
    \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
    $total = Cart::total(2,'.','') * 100;
    $intent = \Stripe\PaymentIntent::create([
      'amount' => $total,
      'currency' => 'eur',
      // Verify your integration in this guide by including this parameter
      'metadata' => ['integration_check' => 'accept_a_payment'],
    ]);
    return $intent;
  }

  	// Function to send new account message
  public function paymentConfirmed($invoice, $user){

    Mail::send('emails.receipt',[
     'user' => $user,
     'invoice' => $invoice
   ], function ($message) use ($invoice, $user)
   {
     $message->from('donotreply@eternumcolumbarium.com', 'Eternum Columbarium');
     $message->subject('Receipt - Eternum Columbarium');
  	 $message->to($user->email);
     //$message->to('luke@elementseven.co');
   });

    Mail::send('emails.adminPurchase',[
     'user' => $user,
     'invoice' => $invoice
   ], function ($message) use ($invoice, $user)
   {
     $message->from('donotreply@eternumcolumbarium.com', 'Eternum Columbarium');
     $message->subject('New Purchase - Eternum Columbarium');
     $message->to('info@eternumcolumbarium.com');
     //$message->to('luke@elementseven.co');
   });

    return "success";
  }

  private function accountCreated($user)
  { 
    Mail::send('emails.accountCreated',[
      'user' => $user
    ], function ($message) use ($user){

      $message->from('donotreply@eternumcolumbarium.com', 'Eternum Columbarium');
      $message->subject('Account Created');
      $message->to($user->email);
      //$message->to('luke@elementseven.co');
    });
    return true;
  }


    // Display shop page
  public function shop(){
    $items = Item::where('status', 'available')->get();
    return view('shop.index')->with(['items' => $items]);
  }

    // Display basket page
  public function basket(){
    return view('shop.basket');
  }

    // Display checkout page
  public function checkout(){
    $billingaddress = null;
    $shippingaddress = null;
    if(Auth::check() && count(Auth::user()->addresses)){
      $billingaddress = Address::where([['user_id', '=', Auth::id()], ['type', '=', 'billing']])->first();
      $shippingaddress = Address::where([['user_id', '=', Auth::id()], ['type', '=', 'shipping']])->first();
    }

    $shipping = 0;
    foreach(Cart::content() as $cc){
       if($cc->options->type == 'physical'){
        $shipping = $shipping + ($cc->model->shipping * $cc->qty);
      }
    }
    return view('shop.checkout')->with(['shipping' => $shipping, 'billingaddress' => $billingaddress, 'shippingaddress' => $shippingaddress]);
  }

    // Display success page
  public function success($transaction_id){
    $invoice = Invoice::where('transaction_id', $transaction_id)->first();
    return view('shop.success')->with(['invoice' => $invoice]);
  }


  public function remove($rowId){

    Cart::remove($rowId);
    if(Cart::count() == 0){
     Cart::destroy();
   }
   return redirect()->to("/basket");
  }


  // Add an item to the basket
  public function addToBasket(Position $position, Lease $lease){
  	// Add the item to the cart
    Cart::add( $position->id, 'Row '.$position->row . ' -  Col '.$position->column . ' -  Position '.$position->position, 1, $lease->price, ['image'=>$lease->partner->getFirstMediaUrl('partners','thumbnail'), 'lease'=> $lease])->associate('App\Position');
   
    // $position->lease_id = $lease->id;
    // $position->save();

  		// Set the tax rate for each cart item to 0
   $items = Cart::content();
   $tax_rate = 13.5;
   config( ['cart.tax' => $lease->tax] );
   foreach ($items as $item){
     $item->setTaxRate($tax_rate);
     Cart::update($item->rowId, $item->qty); 
   }

      // Send the user to the basket
   return redirect()->to('/basket');

  }

  // Empty the basket
  public function emptyBasket(){
    Cart::destroy();
    return redirect()->to("/basket");
  }

  // Empty the basket and redirect to contact
  public function emptyBasketContact(){
    Cart::destroy();
    return redirect()->to("/contact");
  }

  	// Function to create a new invoice and add it to the user
  public function createInvoice($transaction_id, $price, $paid){
    $invoice = Invoice::create([
     'transaction_id'=> $transaction_id,
     'price'=> $price,
     'paid' => $paid
   ]);
    return $invoice;
  }

    // Update the invoice total
  public function updateTotal(Invoice $invoice){
    $invoice->price = number_format((float)Cart::total(), 2, '.', '');
    $invoice->save();
  }

  	// Function to create an address
  public function createAddress($streetAddress, $extendedAddress, $city, $postalCode, $country, $region, $type){

    if(Auth::check()){
      $currentAddresses = Auth::user()->addresses;
      foreach ($currentAddresses as $key => $a) {
        if($type == 'billing' && $a->type == 'billing'){
          if($streetAddress != $a->streetAddress){
            $a->delete();
            $address = Address::create([
              'streetAddress' => $streetAddress,
              'extendedAddress' => $extendedAddress,
              'city' => $city,
              'postalCode' => $postalCode,
              'country' => $country,
              'region' => $region,
              'type' => $type,
              'user_id' => Auth::id()
            ]);
            return $address;
          }else{
            $address = $a;
            return $address;
          }
        }else if($type == 'shipping' && $a->type == 'shipping'){
          if($streetAddress != $a->streetAddress){
            $a->delete();
            $address = Address::create([
              'streetAddress' => $streetAddress,
              'extendedAddress' => $extendedAddress,
              'city' => $city,
              'postalCode' => $postalCode,
              'country' => $country,
              'region' => $region,
              'type' => $type,
              'user_id' => Auth::id()
            ]);
            return $address;
          }else{
            $address = $a;
            return $address;
          }
        }else{
          return 'same';
        }
      }
      if(count($currentAddresses) == 0){
        $address = Address::create([
          'streetAddress' => $streetAddress,
          'extendedAddress' => $extendedAddress,
          'city' => $city,
          'postalCode' => $postalCode,
          'country' => $country,
          'region' => $region,
          'type' => $type,
          'user_id' => Auth::id()
        ]);
        return $address;
      }
    }else{
      $address = Address::create([
        'streetAddress' => $streetAddress,
        'extendedAddress' => $extendedAddress,
        'city' => $city,
        'postalCode' => $postalCode,
        'country' => $country,
        'region' => $region,
        'type' => $type,
        'user_id' => Auth::id()
      ]);
      return $address;
    }

  }

}
