<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interest;
use App\User;
use Mail;
use Auth;

class SendMail extends Controller
{
	public function enquiry(Request $request){

    	// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'message' => 'required|string',
		]); 

		$subject = "General Enquiry";
		$name = $request->input('name');
		$email = $request->input('email');
		$content = $request->input('message');

		$location = null;
		if($request->input('location') != ""){
			$location = $request->input('location');
		}
		$phone = null;
		if($request->input('phone') != ""){
			$phone = $request->input('phone');
		}

		Mail::send('emails.enquiry',[
			'name' => $name,
			'subject' => $subject,
			'email' => $email,
			'location' => $location,
			'content' => $content
		], function ($message) use ($subject, $email, $location, $name, $content){
			$message->from('donotreply@eternumcolumbarium.com', 'Eternum Columbarium');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@eternumcolumbarium.com');
			//$message->to('luke@elementseven.co');
		});
		return 'success';
	}
}
