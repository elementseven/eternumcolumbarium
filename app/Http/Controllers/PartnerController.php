<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partner;
use App\Position;
use App\Lease;
use Cart;

class PartnerController extends Controller
{

  /**
  * Confirm a location.
  */
  public function confirmLocation(Request $request, Partner $partner){
    return view('order.locationConfirmed')->with(['location' => $partner]);
  }

  /**
  * Confirm a lease.
  */
  public function confirmLease(Request $request, Lease $lease){
    return view('order.leaseConfirmed')->with(['location' => $lease->partner, 'lease' => $lease]);
  }

  /**
  * Get positons.
  */
  public function getPostions(Partner $partner){
    $taken = array();
    foreach(Cart::content() as $c){
      array_push($taken, $c->id);
    }
    $positions = Position::where('partner_id', $partner->id)->where('user_id', null)->whereNotIn('id', $taken)->where('lease_id', null)->orderBy('row','asc')->orderBy('column','asc')->get();
    return $positions;
  }

  /**
  * Get locations.
  */
  public function get(Request $request){
  	$locations = Partner::where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
    ->orWhere([['location','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
    ->orderBy('created_at','desc')
    ->paginate($request->input('limit'));
    foreach($locations as $p){
      $p->normal = $p->getFirstMediaUrl('partners', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('partners', 'normal-webp');
      $p->mimetype = $p->getFirstMedia('partners')->mime_type;
    }
    return $locations;
  }
}
