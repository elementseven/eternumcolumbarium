<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Memorial;
use App\Partner;
use App\Position;
use App\Post;
use Auth;
use Mail;

class PageController extends Controller
{
  public function welcome(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    $memorials = Memorial::where('status','published')->orderBy('created_at','desc')->paginate(4);
    $partners = Partner::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('welcome', compact('posts','memorials','partners'));
  }

  public function partnerships(){
    return view('partnerships');
  }

  public function faqs(){
    return view('faqs');
  }
  public function tandcs(){
    return view('tandcs');
  }
  public function privacyPolicy(){
    return view('privacy');
  }
  public function about(){
    return view('about');
  }
  public function memorials(){
    return view('memorials.index');
  }
  public function memorialsShow($slug){
    $memorial = Memorial::where('slug',$slug)->where('status','published')->first();
    return view('memorials.show',compact('memorial'));
  }
  public function locations(){
    return view('locations.index');
  }
  public function locationsShow($slug){
    $partner = Partner::where('slug', $slug)->where('status','published')->first();
    return view('locations.show',compact('partner'));
  }
  public function contact(){
  	return view('contact');
  }
  public function news(){
    $categories = Category::has('posts')->orderBy('name','asc')->get();
    return view('news.index', compact('categories'));
  }
  public function newsShow($slug){
  	$post = Post::where('slug',$slug)->where('status','published')->first();
  	$others = Post::where('id','!=', $post->id)->where('status', 'published')->orderBy('created_at','desc')->paginate(6);
  	return view('news.show',compact('post','others'));
  }
  // Return news as an array
  public function getPosts(Request $request){
    if($request->input('category') == '*'){
      $posts = Post::where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'));
    }else{
      $posts = Post::orderBy('created_at','desc')->whereHas('categories', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'));
    }
    foreach($posts as $p){
      $p->featured = $p->getFirstMediaUrl('blogs', 'normal');
      $p->featuredwebp = $p->getFirstMediaUrl('blogs', 'normal-webp');
      $p->mimetype = $p->getFirstMedia('blogs')->mime_type;
    }
    return $posts;
  }
  // Return memorials as an array
  public function getMemorials(Request $request){
    $memorials = Memorial::where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
    ->orderBy('created_at','desc')
    ->paginate($request->input('limit'));
    foreach($memorials as $p){
      $p->normal = $p->getFirstMediaUrl('memorials', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('memorials', 'normal-webp');
      $p->mimetype = $p->getFirstMedia('memorials')->mime_type;
    }
    return $memorials;
  }
  // Return locations as an array
  public function getLocations(Request $request){
    $locations = Partner::where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
    ->orderBy('created_at','desc')
    ->paginate($request->input('limit'));
    foreach($locations as $p){
      $p->normal = $p->getFirstMediaUrl('partners', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('partners', 'normal-webp');
      $p->mimetype = $p->getFirstMedia('partners')->mime_type;
    }
    return $locations;
  }

  public function addPositions(Partner $partner, $rows, $cols){
    $alpha = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    for($r = 1; $r <= $rows; $r++){
      for($c = 1; $c <= $cols; $c++){
        for($p = 1; $p <= 2; $p++){
          Position::create([
            'row' => $alpha[$r - 1],
            'column' => $c,
            'position' => $p,
            'partner_id' => $partner->id
          ]);
        }
      }
    }
    return "added";
  }
}
