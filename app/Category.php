<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name','slug'
    ];
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($category) {
            $slug = strtolower($category->name);
            //Make alphanumeric (removes all other characters)
            $slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);
            //Clean up multiple dashes or whitespaces
            $slug = preg_replace("/[\s-]+/", " ", $slug);
            //Convert whitespaces and underscore to dash
            $slug = preg_replace("/[\s_]/", "-", $slug);

            $category->slug = $slug;
        });
    }
    public function posts(){
        return $this->belongsToMany('App\Post', 'category_post')->withPivot('post_id');
    }
}
