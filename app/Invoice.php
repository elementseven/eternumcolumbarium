<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
  protected $fillable = [
    'transaction_id','price','paid','user_id'
  ];
  public function user(){
  	return $this->belongsTo('App\User');
  }
  public function positions(){
    return $this->hasMany('App\Position');
  }
}
