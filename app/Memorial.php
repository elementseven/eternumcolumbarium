<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon;

class Memorial extends Model implements HasMedia
{
	use HasMediaTrait;

	protected $dates = [
    'death','birth', 'created_at', 'updated_at'
	];

	protected static function boot()
	{
		parent::boot();
    static::saving(function ($post) {
			$date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->death)->format('Y-m-d');
      $slug = $date .'-'. Str::slug($post->location, "-") .'-'. Str::slug($post->title, "-");
      $post->slug = $slug;
    });
	}

	protected $casts = [
		'created_at' => 'datetime',
	];

	public function registerMediaConversions(Media $media = null)
	{
		$this->addMediaConversion('normal')->width(600)->crop('crop-center', 600, 600);
		$this->addMediaConversion('normal-webp')->width(600)->crop('crop-center', 600, 600)->format('webp');
		$this->addMediaConversion('double')->width(1000)->crop('crop-center', 1000, 1000);
		$this->addMediaConversion('double-webp')->width(1000)->crop('crop-center', 1000, 1000)->format('webp');
		$this->addMediaConversion('thumbnail')->crop('crop-center', 400, 400);
		$this->addMediaConversion('featured')->width(250)->crop('crop-center', 250, 250);
		$this->addMediaConversion('featured-webp')->width(250)->crop('crop-center', 250, 250)->format('webp');
	}
	public function registerMediaCollections()
	{
		$this->addMediaCollection('memorials')->singleFile();
	}
}
