<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
  protected $fillable = [
    'row','column','position', 'partner_id', 'user_id', 'invoice_id', 'lease_id'
  ];
  public function invoice(){
    return $this->belongsTo('App\Invoice');
  }
  public function partner(){
    return $this->belongsTo('App\Partner');
  }
  public function user(){
    return $this->belongsTo('App\User');
  }
  public function lease(){
    return $this->belongsTo('App\Lease');
  }
}
