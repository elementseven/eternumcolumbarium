<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
	    'streetAddress', 'extendedAddress', 'city', 'postalCode', 'country', 'region', 'company', 'type', 'user_id'
	];
	public function user(){
	    return $this->belongsTo('App\User');
	}
}
