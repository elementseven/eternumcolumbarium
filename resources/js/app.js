/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
window.Vue = require('vue');
window.moment = require('moment');
require('./plugins/modernizr-custom.js');
require('./plugins/cookieConsent.js');
require('waypoints/lib/jquery.waypoints.min.js');

require('./a-general.js');
require("babel-polyfill");
import VueLazyload from 'vue-lazyload';
import vueDebounce from 'vue-debounce';
Vue.use(VueLazyload);
Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup']
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('mailing-list', require('./components/MailingList.vue').default);
Vue.component('news-index', require('./components/news/Index.vue').default);
Vue.component('news-show', require('./components/news/Show.vue').default);
Vue.component('memorials-index', require('./components/memorials/Index.vue').default);
Vue.component('memorials-show', require('./components/memorials/Show.vue').default);
Vue.component('faqs', require('./components/Faqs.vue').default);
Vue.component('contact-page-form', require('./components/ContactPageForm.vue').default);
Vue.component('partnership-form', require('./components/PartnershipForm.vue').default);
Vue.component('locations-index', require('./components/locations/Index.vue').default);
Vue.component('locations-show', require('./components/locations/Show.vue').default);
Vue.component('select-location', require('./components/Order/SelectLocation.vue').default);
Vue.component('select-position', require('./components/Order/SelectPosition.vue').default);
Vue.component('countries', require('./components/Countries.vue').default);
Vue.component('checkout', require('./components/Checkout/Index.vue').default);
Vue.component('personal-details', require('./components/Checkout/PersonalDetails.vue').default);
Vue.component('address-details', require('./components/Checkout/Address.vue').default);
Vue.component('payment-details', require('./components/Checkout/Payment.vue').default);
Vue.component('stages-bar', require('./components/Checkout/Stages.vue').default);
Vue.component('footer-top', require('./components/FooterTop.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
