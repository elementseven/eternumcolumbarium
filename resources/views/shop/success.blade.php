@php
$page = 'Success';
$pagetitle = 'Success | Eternum Columbarium';
$metadescription = 'Success - Eternum Columbarium';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
$tax = 0.00;
foreach($invoice->positions as $p){
  $tax = $tax + $p->lease->tax;
}

@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mt-5 mob-mt-0">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5">
      <div class="pt-5">
        <p class="text-primary text-small text-uppercase letter-spacing mb-2" data-aos="fade-in"><b>Thank you!</b></p>
        <h1 class="pb-5 mb-0" data-aos="fade-in" data-aos-delay="100">Payment Successful</h1>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container">
  <div class="row pb-5">
    <div class="col-12">
      <div class="pb-5 mb-5">
        <h3 class="mb-4">Thank you for your payment</h3>
        <p class="text-large mb-5">You will receive an email to <span class="text-primary">{{$invoice->user->email}}</span> confirming your login and order details shortly. <br/><u>Please remember to check your inbox and your spam folder</u>.</p>
         
        @if(count($invoice->positions))<h3 class="mt-5">Columbarium Positions:</h3>@endif

        @foreach($invoice->positions as $key => $position)
        <div class="row">
          <div class="col-12">
            <p class="text-large"><b>{{$position->partner->title}}, {{$position->partner->location}}</b><br>Row {{$position->row}}, Position {{$position->column}}<br/>{{$position->lease->title}}</p>
            @if($key != count($invoice->positions) - 1)<hr/>@endif
          </div>
        </div>
        @endforeach
        <div class="row mt-5"> 
          @foreach($invoice->user->addresses as $address)
          <div class="col-lg-4 col-md-6">
            <h4 class="text-capitalize">{{$address->type}} address</h4>
            <p class="text-large">{{$address->streetAddress}},<br/>@if($address->extendedAddress){{$address->extendedAddress}},<br/>@endif @if($address->region){{$address->region}},<br/>@endif{{$address->city}},{{$address->postalCode}},<br/>{{$address->country}}</p>
          </div>
          @endforeach
        </div>
        <p class="mt-4 mb-0"><b>Tax:</b> €{{number_format($tax, 2)}}</p>
        <h3 class="mt-1">Total Paid:</b> €{{number_format($invoice->price, 2)}} </h3>
        <hr class="line primary-line my-5"/>
        <p class="text-large mb-2">We will be in touch shortly to make any necessary arrangements.</p>
        <a href="/login">
          <div class="btn btn-primary">Login</div>
        </a>
      </div>   
    </div>
  </div>
</div>
@endsection