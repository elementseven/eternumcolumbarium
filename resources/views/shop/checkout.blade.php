@php
$page = 'Checkout';
$pagetitle = 'Checkout | Eternum Columbarium';
$metadescription = 'Checkout - Eternum Columbarium';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])

@php 
	$hasAddress = false;
	$sameAddress = false;
	$billingAddress = null;
	$shippingAddress = null;
	if(Auth::check()){
		if(count($currentUser->addresses)){
			$hasAddress = true;
			foreach($currentUser->addresses as $a){
				if($a->type == 'billing'){
					$billingAddress = $a;
				}else if($a->type == 'shipping'){
					$shippingAddress = $a;
				}
			}
			if($billingAddress && $billingAddress->streetAddress == $shippingAddress->streetAddress){
				$sameAddress = true;
			}
		}
	}
	$hasMerch = false;
	
@endphp

@section('header')
<header class="container position-relative pt-5 mob-pt-0">
	<div class="row pt-5 text-center text-lg-left">
		<div class="col-lg-12 mt-5">
			<h1 class="page-title pt-5">Checkout</h1>
			<img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
		</div>
	</div>
</header>
@endsection

@section('content')
@if($shippingaddress && $billingaddress)
<checkout :amount="{{Cart::total(2,'.','')}}" :stripekey="'{{ env("STRIPE_KEY") }}'" login="{{Auth::check()}}" :billingaddress="{{$billingaddress}}" :shippingaddress="{{$shippingaddress}}"></checkout>
@else
<checkout :amount="{{Cart::total(2,'.','')}}" :stripekey="'{{ env("STRIPE_KEY") }}'" login="{{Auth::check()}}"></checkout>
@endif
@endsection

@section('prescripts')
<script src="https://js.stripe.com/v3/"></script>
@endsection
@section('scripts')
@endsection
