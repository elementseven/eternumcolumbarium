@php
$page = 'Basket';
$pagetitle = 'Basket | Eternum Columbarium';
$metadescription = 'Basket - Eternum Columbarium';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mob-pt-0">
	<div class="row pt-5 text-center text-lg-left">
		<div class="col-lg-12 mt-5 mb-5">
			@if(count(Cart::content()))
			<h1 class="page-title pt-5">Basket</h1>
			<img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
			<p><b>Want to add another space to your basket? <a href="{{url()->previous()}}">Click here</a></b></p>
			@else
			<h1 class="page-title pt-5 mb-0">Basket Empty</h1>
			<img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
			<p class="mb-4">You have nothing in your basket! Select a location to continue.</p>
			<a href="{{route('locations')}}">
				<div class="btn btn-primary btn-icon mb-5">View locations <i class="fa fa-chevron-right"></i></div>
			</a>
			@endif
		</div>
	</div>
</header>
@endsection
@section('content')
@if(count(Cart::content()))
<div class="container mt-5 pb-5  mob-mt-0">
	<div class="row mt-5 mob-mt-0">
		<div class="col-12">
			@foreach(Cart::content() as $key => $i)
			<div class="row half_row mb-3">
				<div class="col-lg-1 col-3 half_col">
					<div class="square bg" style="background-image: url('{{$i->options->image}}');"></div>
				</div>
				<div class="col-lg-7 col-9 half_col">
					<h4 class="basket-item-title mb-0">{{$i->model->partner->title}}</h4>
					<p class="mb-0 text-small">{{$i->options->lease->title}}</p>
					<p class="text-small mb-0">{{$i->name}}</p>
					<p class="d-none d-lg-block mb-0">{{$i->model->excerpt}}</p>
					<div class="d-lg-none">
						<p class="mb-0 mob-mt-0 text-dark text-small"><span class="text-dark">€{{number_format((float)$i->price, 2, '.', '')}}</span> &nbsp; <a href="{{route('shop.removefrombasket',['rowId' => $i->rowId])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
					</div>
					<p class="mob-mb-0 d-none d-lg-block text-small"><a href="{{route('shop.removefrombasket',['rowId' => $i->rowId])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
				</div>
				<div class="col-lg-4 d-none d-lg-block half_col text-lg-right">
					<p class="mb-0 mob-mt-0 text-large text-dark"><span class="text-dark">€{{number_format((float)$i->price, 2, '.', '')}}</span></p>
				</div>
			</div>
			@endforeach
		</div>
		<div class="col-md-12 text-right">
			@if(count(Cart::content()) > 0)
			<a href="/empty-basket" class="grey-link"><i class="fa fa-trash-o"></i> Empty Basket</a>
			@endif
			<hr class="line primary-line"/>
		</div>
	</div>
	<div class="row mt-5 mob-mt-0">
		<div class="col-12 text-center text-md-right mob-text-left mob-mt-3">
			<p class="text-center mt-3 mb-0 text-md-right"><b><span class="text-primary">VAT: </span> €{{Cart::tax()}}</b></p>
			<h3 class="text-center mt-1 text-md-right"><span class="text-primary">Total: </span> €{{Cart::total()}}</h3>
			<a href="{{route('checkout')}}">
				<div class="btn btn-primary mt-2 mx-auto mb-3 float-right mob-float-none btn-icon">Checkout <i class="fa fa-chevron-right"></i></div>
			</a>
			{{-- <a href="/checkout">
				<div class="btn btn-primary mt-2 mx-auto mb-3 float-right mob-float-none btn-icon">Contact Us <i class="fa fa-chevron-right"></i></div>
			</a> --}}
		</div>
		@if(!Auth::check())
		<div class="col-lg-9 offset-lg-3">
			<p class="text-center text-md-right">The columbarium in Ennis Cathedral will be installed in September and payments will be taken then. In the meantime, please <a href="/contact">contact us</a> to reserve a niche.</p>
			<p class="text-center text-md-right">If you have already purchased a place in one of our columbaria, please <a href="/login?redirect=basket"><b>login</b></a> before you checkout</p>
		</div>
		@else
		<div class="col-12">
			<p class="text-center text-md-right mb-0">You are logged in as <b>{{$currentUser->full_name}}</b>.<br>Purchases will be added to your account.</p>
			<p class="text-center text-md-right"><a href="/not-you">Not you? Click here to log out.</a></p>
		</div>
		@endif
	</div>
</div>
@else
<footer-top></footer-top>
@endif
@endsection
@section('scripts')
@endsection