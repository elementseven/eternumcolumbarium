@php
$page = 'Customer Dashboard';
$pagetitle = 'Customer Dashboard - Eternum Columbarium | Columbarium Solutions for Ireland & The UK';
$metadescription = '';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="container pt-5">
  <div class="row pt-5">
    <div class="col-12 mt-5">
      <p class="mb-0">Customer Dashboard</p>
      <h2 class="">Welcome {{$currentUser->first_name}}!</h2>
    </div>
  </div>
</div>
@endsection
@section('content')
<div class="container pb-5">
  <div class="row py-5">
    <div class="col-12 mt-5">
      <h4 class="mb-3">Your Purchases</h4>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Transaction Id</th>
              <th scope="col">Date</th>
              <th scope="col">Price</th>
              <th scope="col">Location & Positions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($currentUser->invoices as $i)
            <tr>
              <th scope="row">{{$i->transaction_id}}</th>
              <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $i->created_at)->format('d/m/Y')}}</td>
              <td>£{{$i->price}}</td>
              <td>
                @foreach($i->positions as $p)
                <b>{{$p->partner->title}}</b> Row: {{$p->row}}, Col: {{$p->colum}}, Postition: {{$p->position}} of 2<br/>
                @endforeach
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="container pb-5">
    <div class="row pb-5">
      <div class="col-12">
        <h3>Got a question?</h3>
        <p>Please quote your transaction id in your message</p>
      </div>
      <div class="col-lg-12 position-relative z-2 overflow-hidden mob-px-3">
        <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'" :lochide="true"></contact-page-form>
      </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection