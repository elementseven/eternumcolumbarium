@php
$tax = 0.00;
foreach($invoice->positions as $p){
  $tax = $tax + $p->lease->tax;
}
@endphp

<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:300; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px;"><img src="https://eternumcolumbarium.com/img/logos/logo.png" width="250px" alt="Eternum Columbarium Logo"/></p>

				<p style="font-size:22px; background-color: #b39e7a; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Receipt</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi Joseph,</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">{{$user->full_name}} has purchased a place in one of your columbaria, details below.</p>
				
				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Name:</b> {{$user->full_name}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Payment reference:</b> {{$invoice->transaction_id}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;"><b>Positions:</b></p>

				@foreach($invoice->positions as $position)
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;"><b>{{$position->partner->title}}, {{$position->partner->location}}</b><br> Row {{$position->row}}, Column {{$position->column}}, Position {{$position->position}} of 2</p>
				@endforeach

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>VAT:</b> €{{number_format($tax, 2)}}</p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Total Paid:</b> €{{number_format($invoice->price, 2)}} </p>

				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				</td>
				</tr>
				<tr><td>
				<img src="https://eternumcolumbarium.com/img/logos/logo.png" width="250px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the <a href="https://eternumcolumbarium.com">Eternum Columbarium website</a><br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>