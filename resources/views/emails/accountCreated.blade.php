<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #000;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:300; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px 40px 0;">
					<img src="https://eternumcolumbarium.com/img/logos/logo.png" width="150" style="margin: auto; display: block;"/>
				</p>

				<p style="font-size:22px; background-color: #b39e7a; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Account Created</span></p>

				<p style="font-size:18px; color:#000; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi {{$user->first_name}},</span></p>

				<p style="font-size:18px; color:#000; text-align: left; font-family:'Arial', arial, sans-serif;">Your Eternum Columbarium account has been created for you, you can <a href="https://eternumcolumbarium.com/login" style="color: #b39e7a;">login here</a>.</p>

				<p style="font-size:18px; color:#000; text-align: left; font-family:'Arial', arial, sans-serif;">Please use the email address and password You used when signing up.</p>

				</td>
				</tr>
				<tr><td>
				<p style="border-bottom: 1px solid #000; margin: 40px auto;"></p>
				<img src="https://eternumcolumbarium.com/img/logos/logo.png" width="150px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#000; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the <a href="https://eternumcolumbarium.com">Eternum Columbarium Website</a>.<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>