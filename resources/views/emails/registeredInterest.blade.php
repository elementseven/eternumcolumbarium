<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px 40px 0;">
					<img src="https://fibrus.com/img/logos/logo.png" width="150" style="margin: auto; display: block;"/>
				</p>

				<p style="font-size:22px; background-color: #2FC251; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">{{$subject}}</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi, {{$interest->first_name}}</span></p>
				
				@if($interest->type == 'residential')
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thank you for registering your interest in Fibrus. We will be in touch with details on how to sign up once we are operating in your area.</p>
				@else
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thank you for registering {{$interest->company}}'s interest in Fibrus. We will be in touch with details on how to sign up once we are operating in your area.</p>
				@endif
				</td>
				</tr>
				<tr><td>
					<hr style="margin: 30px auto 0;">
				<img src="https://fibrus.com/img/logos/logo.png" width="120" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the <a href="https://fibrus.com">Fibrus website</a>.<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>