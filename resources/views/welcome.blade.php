@php
$page = 'Homepage';
$pagetitle = 'Eternum Columbarium | Columbarium Solutions for Ireland & The UK';
$metadescription = '';
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative full-height home-top-1 bg">
  <div id="home-slider" class="slideshow" data-ride="carousel">
    <div class="slideshow-image active home-top-1 bg-20"></div>
    <div class="slideshow-image home-top-2 bg-90"></div>
    <div class="slideshow-image home-top-4 bg-60"></div>
    <div class="slideshow-image home-top-3 bg-20"></div>
  </div>
  <div class="trans"></div>
  <div class="row justify-content-center">
    <div class="col-lg-7 text-center text-lg-left">
      <div class="d-table w-100 full-height">
        <div class="d-table-cell align-middle w-100 full-height text-center">
          <img src="/img/temp/logo-symbol.png" class="home-top-dove" alt="Dove temporay image"/>
          <h1 class="page-top mb-5 text-white ipad-mb-3 mob-mb-4">Sacred resting places for cremated remains</h1>
          <a href="/locations">
            <div class="btn btn-primary btn-big btn-icon">View Locations <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="container py-5 my-5 mob-pb-0 mob-px-4">
      <div class="row justify-content-center">
        <div class="col-lg-7 mob-mb-5">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100 text-center text-md-left">
              <p class="text-larger">Eternum works in partnership with local churches and organisations to offer beautiful resting places for cremated remains. Our columbaria offer a dignified, secure and spiritual space where the memory of your loved ones is celebrated and honoured.</p>
              <p class="text-larger mb-4">Expertly crafted in Ireland, our beautiful columbarium vaults echo the ancient Christian tradition of churchyard burial and offer the opportunity to be interred in familiar surroundings in an active centre of faith.</p>
              <a href="/locations">
                <div class="btn btn-primary btn-icon">View Locations <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-10 d-none d-lg-block">
          <img src="/img/temp/logo-symbol.png" class="w-100" alt="Dove temporay image"/>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid bg1 bg bg-fixed py-5 mob-py-0 position-relative">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container py-5 text-white text-center">
      <div class="row">
        <div class="col-lg-4">
          <img src="/img/shapes/church.svg" alt="Columbariums in church Northern Ireland" height="80" class="mb-3" />
          <h4>LOCAL</h4>
          <p class="text-large mb-0">Rest in a familiar church</p>
        </div>
        <div class="col-lg-4 mob-mt-5 mob-py-5">
          <img src="/img/shapes/cross.svg" alt="Cross in church Northern Ireland" height="80" class="mb-3" />
          <h4>SACRED</h4>
          <p class="text-large mb-0">At the centre of faith</p>
        </div>
        <div class="col-lg-4 mob-mt-5">
          <img src="/img/shapes/family.svg" alt="Family in church Northern Ireland" height="80" class="mb-3" />
          <h4>SECURE</h4>
          <p class="text-large mb-0">Comfort for you and your loved ones</p>
        </div>
      </div>
    </div>
  </div>
</div>

    <div class="container py-5 mob-py-3 mob-px-4">
      <div class="row py-5 justify-content-center">
        <div class="col-12 text-center mb-4">
          <h2 class="mb-4 grey-text">Partnered Organisations</h2>
          <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic"/>
        </div>
        @foreach($partners as $key => $partner)
        <div class="col-md-6 @if($key != count($partners) -1) mb-5 @endif">
          <div class="zoom-link" data-aos="fade-in">
            <a href="{{route('locations-single', ['slug' => $partner->slug])}}">
              <div class="rounded-image zoom-img">
                <picture> 
                  <source media="(min-width: 900px)" srcset="{{$partner->getFirstMediaUrl('partners', 'normal')}} 1x, {{$partner->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
                  <source media="(min-width: 601px)" srcset="{{$partner->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$partner->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
                  <source srcset="{{$partner->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$partner->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
                  <img srcset="{{$partner->getFirstMediaUrl('partners', 'normal')}} 600w, {{$partner->getFirstMediaUrl('partners', 'double')}} 900w, {{$partner->getFirstMediaUrl('partners', 'double')}} 1440w" src="{{$partner->getFirstMediaUrl('partners', 'double')}}" type="{{$partner->getFirstMedia('partners')->mime_type}}" alt="{{$partner->title}}" class="w-100" />
                </picture>
              </div>
              <div class="text-dark mt-3">
                <p class="post-exerpt text-small mb-1"><i class="fa fa-map-marker text-primary mr-2"></i><b>{{$partner->location}}</b></p>
                <h4 class="post-title">{{$partner->title}}</h4>
                <p class="mb-0 text-small"><b>Read more</b> <i class="fa fa-arrow-circle-right ml-1"></i></p>
              </div>
            </a>
          </div>
        </div>
        @endforeach
        <div class="col-12 text-center mt-5">
          <a href="{{route('locations')}}">
            <div class="btn btn-primary btn-icon">All Locations <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>

<div class="container-fluid bg-light">
  <div class="row">
    {{-- <select-location></select-location> --}}
    <div class="container-fluid">
      <div class="row">
        <div class="container py-5 mob-py-3 mob-px-4">
          <div class="row py-5 justify-content-center">
            <div class="col-lg-8 text-center">
              <h2 class="mb-4 grey-text">Become a Partner</h2>
              <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic"/>
              <p class="text-large mt-3 mb-4">Would you like more information on how a partnership with Eternum would benefit your parish or organisation?</p>
              <a href="/partnerships">
                <div class="btn btn-primary btn-big btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- <div class="container py-5 mob-px-4">
  <div class="row py-5 mob-py-3">
    <div class="col-12 text-center mb-4">
      <h2 class="mb-4 grey-text">Memorials</h2>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic"/>
    </div>
    @foreach($memorials as $key => $memorial)
    <div class="col-lg-6 @if($key != count($memorials) -1) mb-5 mob-mb-4 @endif">
      <a href="{{route('memorials-single', ['slug' => $memorial->slug])}}">
      <div class="row half_row">
        <div class="col-lg-4 col-4 half_col">
      
          <div class="memorial-image">
            <picture> 
              <source media="(min-width: 900px)" srcset="{{$memorial->getFirstMediaUrl('memorial', 'normal')}} 1x, {{$memorial->getFirstMediaUrl('memorials', 'double-webp')}} 2x" type="image/webp"/> 
              <source media="(min-width: 601px)" srcset="{{$memorial->getFirstMediaUrl('memorials', 'normal-webp')}} 1x, {{$memorial->getFirstMediaUrl('memorials', 'double-webp')}} 2x" type="image/webp"/> 
              <source srcset="{{$memorial->getFirstMediaUrl('memorials', 'normal-webp')}} 1x, {{$memorial->getFirstMediaUrl('memorials', 'double-webp')}} 2x" type="image/webp"/> 
              <img srcset="{{$memorial->getFirstMediaUrl('memorials', 'normal')}} 600w, {{$memorial->getFirstMediaUrl('memorials', 'double')}} 900w, {{$memorial->getFirstMediaUrl('memorials', 'double')}} 1440w" src="{{$memorial->getFirstMediaUrl('memorials', 'double')}}" type="{{$memorial->getFirstMedia('memorials')->mime_type}}" alt="{{$memorial->title}}" class="w-100" />
            </picture>
          </div>
        </div>
        <div class="col-lg-8 col-8 half_col text-dark mob-pt-2">
          <p class="text-title text-larger mb-0"><b>{{$memorial->title}}</b></p>
          @if($memorial->birth && $memorial->death)
          <p class="text-small mb-1">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $memorial->birth)->format('d/m/Y')}} - {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $memorial->death)->format('d/m/Y')}}</p>
          @endif
          <p class="post-exerpt text-small mb-2 d-none d-md-block">{{$memorial->excerpt}}</p>
          <p class="mb-0 text-small"><b>View memorial</b> <i class="fa fa-arrow-circle-right text-primary ml-1"></i></p>
        </div>
      </div>
      </a>
    </div>
    @endforeach
    <div class="col-12 text-center mob-mt-5">
      <a href="{{route('memorials')}}">
        <div class="btn btn-primary btn-icon">All Memorials <i class="fa fa-chevron-right"></i></div>
      </a>
    </div>
  </div>
</div> --}}
{{-- <div class="container-fluid bg2 bg bg-fixed py-5 position-relative">
  <div class="trans"></div>
  <div class="row">
    <div class="container py-5 text-white text-center">
      <div class="row">
        <div class="col-12">
          <h2 class="mb-4">Interested in a Columbarium?</h2>
          <a href="{{route('locations')}}">
            <div class="btn btn-white btn-icon">Get Started <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div> --}}
<div class="container pb-5 mob-px-4">
  <div class="row py-5 mob-py-3 mt-5 mob-mt-0">
    <div class="col-12 text-center mb-4">
      <h2 class="mb-4 grey-text">Latest News</h2>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic"/>
    </div>
    @foreach($posts as $post)
    <div class="col-md-4 mb-5">
      <a href="{{route('news-single', ['slug' => $post->slug])}}">
        <div class="card border-0 shadow overflow-hidden post-box text-center text-md-left text-dark">
          <div class="post-image">
            <picture> 
              <source media="(min-width: 900px)" srcset="{{$post->getFirstMediaUrl('blogs', 'normal')}} 1x, {{$post->getFirstMediaUrl('blogs', 'double-webp')}} 2x" type="image/webp"/> 
              <source media="(min-width: 601px)" srcset="{{$post->getFirstMediaUrl('blogs', 'normal-webp')}} 1x, {{$post->getFirstMediaUrl('blogs', 'double-webp')}} 2x" type="image/webp"/> 
              <source srcset="{{$post->getFirstMediaUrl('blogs', 'normal-webp')}} 1x, {{$post->getFirstMediaUrl('blogs', 'double-webp')}} 2x" type="image/webp"/> 
              <img srcset="{{$post->getFirstMediaUrl('blogs', 'normal')}} 600w, {{$post->getFirstMediaUrl('blogs', 'double')}} 900w, {{$post->getFirstMediaUrl('blogs', 'double')}} 1440w" src="{{$post->getFirstMediaUrl('blogs', 'double')}}" type="{{$post->getFirstMedia('blogs')->mime_type}}" alt="{{$post->title}}" class="w-100" />
            </picture>
          </div>
          <div class="p-4">
            <h4 class="post-title mb-3">{{$post->title}}</h4>
            <p class="post-exerpt text-small mb-3">{{$post->excerpt}}</p>
            <p class="mb-0"><b>Read more</b> <i class="fa fa-arrow-circle-right text-primary ml-1"></i></p>
          </div>
        </div>
      </a>
    </div>
    @endforeach
    <div class="col-12 text-center">
      <a href="{{route('news')}}">
        <div class="btn btn-primary btn-icon">More News <i class="fa fa-chevron-right"></i></div>
      </a>
    </div>
  </div>
</div>
<footer-top></footer-top>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection