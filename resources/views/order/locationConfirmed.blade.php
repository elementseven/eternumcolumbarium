@php
$page = 'Select Lease';
$pagetitle = 'Select Lease | Eternum Columbarium';
$metadescription = 'Select Lease - Columbarium Solutions for Ireland & The UK';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 text-center">
      <h1 class="mt-5 mob-mt-0 faq-title">Select a lease period</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
    </div>
  </div>
</header>
@endsection
@section('content')

<div class="container py-5">
  <div class="row">
    @foreach($location->leases as $lease)
    <div class="col-lg-4">
      <div class="card lease-card px-4 py-5 text-center">
        <h3 class="lease-title">{{$lease->title}}</h3>
        <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3 mx-auto" />
        <div class="lease-body"> 
          {!!$lease->description!!}
        </div>
        <p class="price mt-3">€{{$lease->price}}</p>
        <a href="/confirm-lease/{{$lease->id}}">
          <div class="btn btn-primary btn-icon">Select lease <i class="fa fa-chevron-right"></i></div>
        </a>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="container py-5 mob-pb-0">
  <div class="row justify-content-center mb-5">
    <div class="col-12 text-center mb-4">
      <h2>Location Details</h2>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3 mx-auto" />
    </div> 
    <div class="col-lg-3">
      <div class="rounded-image">
        <picture> 
          <source media="(min-width: 900px)" srcset="{{$location->getFirstMediaUrl('partners', 'normal')}} 1x, {{$location->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <source media="(min-width: 601px)" srcset="{{$location->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$location->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <source srcset="{{$location->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$location->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <img srcset="{{$location->getFirstMediaUrl('partners', 'normal')}} 600w, {{$location->getFirstMediaUrl('partners', 'double')}} 900w, {{$location->getFirstMediaUrl('partners', 'double')}} 1440w" src="{{$location->getFirstMediaUrl('partners', 'double')}}" type="{{$location->getFirstMedia('partners')->mime_type}}" alt="{{$location->title}}" class="w-100" />
        </picture>
      </div>
    </div>
    <div class="col-lg-4">
      <p class="post-exerpt text-small mb-1"><i class="fa fa-map-marker text-primary mr-2"></i><b>{{$location->location}}</b></p>
      <h3>{{$location->title}}</h3>
      <a href="/locations"><i class="fa fa-chevron-left"></i> <b>Change Location</b></a>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection