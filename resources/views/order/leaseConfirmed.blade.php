@php
$page = 'Lease Confirmed';
$pagetitle = 'Lease Confirmed | Eternum Columbarium';
$metadescription = 'Lease Confirmed - Columbarium Solutions for Ireland & The UK';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 text-center">
      <h1 class="mt-5 mob-mt-0 blog-title">Select a position</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container py-5 mb-5 mob-pt-0 mob-mb-0">
  <div class="row text-center text-lg-left">
    <div class="col-lg-8 pr-5 mob-px-3">
      <img src="/storage/{{$location->diagram}}" alt="{{$location->title}} columbarium diagram" class="w-100"/>
      <p class="mt-3">If you have any questions about selecting a niche or position, <a href="/contact">please get in touch</a> and we will be happy to assist.</p>
    </div>
    <div class="col-lg-4">
      <select-position :location="{{$location->id}}" :lease="{{$lease->id}}"></select-position>
    </div>
  </div>
</div>
<div class="container py-5 mb-5 mob-pt-0">
  <div class="row justify-content-center text-center text-lg-left">
    <div class="col-12 text-center mb-4">
      <h2>Location Details</h2>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3 mx-auto" />
    </div> 
    <div class="col-lg-3">
      <div class="rounded-image">
        <picture> 
          <source media="(min-width: 900px)" srcset="{{$location->getFirstMediaUrl('partners', 'normal')}} 1x, {{$location->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <source media="(min-width: 601px)" srcset="{{$location->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$location->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <source srcset="{{$location->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$location->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <img srcset="{{$location->getFirstMediaUrl('partners', 'normal')}} 600w, {{$location->getFirstMediaUrl('partners', 'double')}} 900w, {{$location->getFirstMediaUrl('partners', 'double')}} 1440w" src="{{$location->getFirstMediaUrl('partners', 'double')}}" type="{{$location->getFirstMedia('partners')->mime_type}}" alt="{{$location->title}}" class="w-100" />
        </picture>
      </div>
    </div>
    <div class="col-lg-4">
      <p class="post-exerpt text-small mb-1 mt-3"><i class="fa fa-map-marker text-primary mr-2"></i><b>{{$location->location}}</b></p>
      <h3>{{$location->title}}</h3>
      <p class="text-larger">{{$lease->title}} - €{{$lease->price}}</p>
      <a href="/confirm-location/{{$location->id}}"><i class="fa fa-chevron-left"></i> <b>Change Lease</b></a>
    </div>
  </div>
</div>

@endsection
@section('scripts')
@endsection