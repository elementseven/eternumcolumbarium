@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.news").'">News</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.news.edit', ['post' => $post->id]) .'">Edit</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<edit-news :post="{{$post}}" :image="'{{$post->getFirstMediaUrl('featured')}}'" :categories='{{$categories}}'></edit-news>
@endsection
