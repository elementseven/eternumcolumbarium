@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.news").'">News</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.news.show', ['news' => $post->id]) .'">View</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<show-news :post="{{$post}}" :image="'{{$post->getFirstMediaUrl('featured','big')}}'"></show-news>
@endsection
@section('modals')
<remove-news></remove-news>
@endsection