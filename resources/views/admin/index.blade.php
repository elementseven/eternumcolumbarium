@php
$bcrumb = '<a href="'.route("home").'">Dashboard</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])

@section('content')
<div class="container my-5">
  <div class="row mb-5">
    <div class="col-md-12 mb-5">
      <website-visitors :days="7"></website-visitors>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-md-6">
      <top-referrers :days="7"></top-referrers>
    </div>
    <div class="col-md-6">
      <user-types :days="7"></user-types>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 mb-5">
      <popular-pages :days="7"></popular-pages>
    </div>
  </div>
</div>
@endsection
