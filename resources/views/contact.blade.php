@php
$page = 'Contact';
$pagetitle = 'Contact | Eternum Columbarium';
$metadescription = 'Columbarium Solutions for Ireland & The UK';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'og_image' => $ogimage])
@section('header')
<header class="container position-relative pt-5 ">
  <div class="row justify-content-center pt-5 mob-pt-0">
    <div class="col-lg-8 pt-5 text-center">
        <h1 class="mt-5">Get in touch</h1>
        <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
        <p class="mb-4 text-large">If you have an any questions, we would love to hear from you. You can use the form below to send us a message or find out contact details in the page footer.</p>
    </div>
</div>
</header>
@endsection
@section('content')
<div class="container pb-5">
    <div class="row justify-content-center pb-5">
        <div class="col-lg-10 position-relative z-2 overflow-hidden mob-px-3">
            <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'" :lochide="false"></contact-page-form>
        </div>
    </div>
</div>
<div class="container-fluid py-5 mob-py-0 flowerbg bg position-relative">
  <div class="trans"></div>
  <div class="row py-5 justify-content-center text-center text-lg-left">
    <div class="container py-5">
      <div class="row">
        <div class="col-lg-10 text-white">
          <h2 class="mb-4 smaller">View our beautiful columbaria</h2>
          <a href="{{route('locations')}}">
            <div class="btn btn-primary btn-icon">View locations <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection