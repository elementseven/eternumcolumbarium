@php
$page = 'Terms & Conditions';
$pagetitle = 'Terms & Conditions | Eternum Columbarium';
$metadescription = 'Terms & Conditions - Columbarium Solutions for Ireland & The UK';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5">
      <h1 class="mt-5 mob-mt-0 faq-title">Terms & Conditions</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3 mb-5" />
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mob-pb-0">
  <div class="row">
    <div class="col-12">
      <p class="p1"><strong>The definitions and rules of interpretation in this clause apply in this agreement.</strong></p>
      <ol class="ol1">
        <ol class="ol1">
          <li class="li2">Definitions:</li>
          <li class="li2"><strong>Burial Place</strong>: a sacred place within the island of Ireland in which a grave shall be prepared for the purposes of burying the Remains and the cremated remains of other deceased persons, with the names of all persons buried at such burial place listed on a plaque to be erected. Eternum (N.I.) Limited (<strong>Eternum</strong>) will use reasonable endeavours to source a burial place as close as is practicable to the Columbarium but is unable to guarantee the location of such burial place.&nbsp;</li>
        </ol>
      </ol>
      <p class="p1"><strong>Certificate</strong>: a certificate of grant of right for use of inurnment provided by Eternum to the customer (as described in the Certificate) (the <strong>Customer</strong>).&nbsp;</p>
      <p class="p1"><strong>Columbarium Fee</strong>: the amount of &euro;1,600 plus VAT of 13.5% payable on the date of this Agreement.</p>
      <p class="p1"><strong>Contact Information</strong>: the name, address, telephone number and email address of the Customer and the Nominated Relation, as notified to Eternum by the Customer in writing from time to time.</p>
      <p class="p1"><strong>Cremation</strong>: the reduction, through extreme heat and evaporation, of a human body to its basic elements as a means of preparing the human body for memorialisation.</p>
      <p class="p1"><strong>Deceased Person</strong>: the deceased person (which shall be either the Customer or an Eligible Relation of the Customer) inurned in the Niche. For the avoidance of doubt, the term Deceased Person shall also refer to any unborn child (who, if born, would have been an Eligible Relation of the Customer) following a spontaneous loss of pregnancy, regardless of the term of gestation.</p>
      <p class="p1"><strong>Eligible Relation</strong>: in relation to the Customer (or, in the case of clause 10, the Deceased Person):</p>
      <p class="p3">(i) &nbsp; &nbsp;his or her spouse, parent, sibling, child or grandchild&nbsp;&nbsp;or the spouse of such child or grandchild (including natural, adopted or step-children and grandchildren and the spouses thereof);&nbsp;</p>
      <p class="p3">(ii) &nbsp; &nbsp;any living person in respect of whom they are appointed as attorney pursuant to an enduring or lasting power of attorney;&nbsp;</p>
      <p class="p3">(iii) &nbsp; &nbsp;any deceased person&nbsp;&nbsp;in respect of whom they are the executor or administrator of the estate; or</p>
      <p class="p3">(iv) &nbsp; &nbsp;any other person approved by Eternum, determined on a case by case basis.</p>
      <p class="p1"><strong>Inurnment</strong>: the placement of Remains in the Niche.</p>
      <p class="p1"><strong>Inurnment Date</strong>: the date upon which Inurnment has taken place in the Niche.</p>
      <p class="p1"><strong>Inurnment Right</strong>: the right set out in clause 2.1.</p>
      <p class="p1"><strong>Inurnment Term</strong>: the period during which the Columbarium and the relevant church remain standing.</p>
      <p class="p1"><strong>Niche</strong>: the cremation niche within the Columbarium having the number stated on the Certificate reserved in accordance with this agreement in the name of the Customer.</p>
      <p class="p1"><strong>Nominated Relation</strong>: the person notified to Eternum by the Customer as being their approved point of contact should the Customer not be available to contact (whether by reason of death or otherwise), whose details are as notified to Eternum by the Customer as such in writing from time to time.</p>
      <p class="p1"><strong>Relevant Contacts</strong>: the Customer<strong>&nbsp;</strong>and the Nominated Relation.</p>
      <p class="p1"><strong>Remains</strong>:<strong>&nbsp;</strong>the cremated human remains of the Deceased Person contained in an Urn.</p>
      <p class="p1"><strong>Urn</strong>:<strong>&nbsp;</strong>an urn to be supplied by Eternum to any of the Relevant Contacts on request in advance of Cremation, specifically designed for the sole purpose of holding cremated remains with the same degree of respect given to a human body, in accordance with the Roman Catholic Church&apos;s Code of Canon Law.&nbsp;</p>
      <ol class="ol1">
        <ol class="ol1">
          <li class="li2">Reference to a clause, paragraph or Schedule is to a clause, paragraph or Schedule of or to this agreement, unless the context requires otherwise.</li>
          <li class="li2">A reference to one gender includes a reference to the other gender.</li>
          <li class="li2">Words in the singular include the plural and in the plural include the singular.</li>
          <li class="li2">A reference to a <strong>person</strong> includes an incorporated or unincorporated body.</li>
          <li class="li2">A reference to a statute or statutory provision is a reference to it as it is in force for the time being taking account of any amendment, extension, or re-enactment and includes any subordinate legislation for the time being in force made under it.</li>
          <li class="li2">Unless the context otherwise requires, the words <strong>including</strong> and <strong>include</strong> and words of similar effect shall not be deemed to limit the general effect of the words which precede them.</li>
          <li class="li2">The headings in this agreement are for ease of reference only and shall not affect its construction or interpretation.</li>
        </ol>
        <li class="li4"><strong>grant of right of inurnment</strong>
          <ol class="ol1">
            <li class="li2">Eternum grants to the Customer the right, upon payment of the Columbarium Fee (payable in accordance with clause 3 of this agreement), to place Remains in the Niche at any time during the Inurnment Term.&nbsp;</li>
            <li class="li2">It is the responsibility of the Customer to inform Eternum of any changes to the Contact Information.</li>
            <li class="li2">In the event that Eternum establishes that the Customer has passed away and that the Niche has remained unused for a period of three years from the date of such passing and Eternum has not, during such period, received a request to use the Niche to for the purposes of inurning an Eligible Relation of the Customer, the Inurnment Right shall lapse.</li>
          </ol>
        </li>
        <li class="li4"><strong>COLUMBARIUM fee</strong>
          <ol class="ol1">
            <li class="li2">The Columbarium Fee is payable by the Customer to Eternum on the date hereof. The Niche will not be reserved for the Customer until such time as the Columbarium Fee is paid in full.</li>
            <li class="li2">Upon the Columbarium Fee being paid in full, Eternum shall provide to the Customer a Certificate, which must be retained by the Customer as it constitutes proof of the right to use the Niche and must be produced on Inurnment.</li>
            <li class="li2">The Columbarium Fee shall include the maintenance of the Columbarium, the grant of the Inurnment Right, the cost of one Inurnment, one Urn, one brass plaque to be affixed to the Niche identifying the Deceased Person and the inscription thereof. It shall not include Cremation, funeral costs incurred by a funeral director or crematorium or otherwise, transport costs or any other external costs.</li>
          </ol>
        </li>
        <li class="li4"><strong>no rights of property</strong></li>
      </ol>
      <p class="p1">The right to use the Niche granted pursuant to clause 2.1 is not intended, and shall not be construed as, to grant any legal title to, or easements upon, the Niche or any space in the Columbarium or the grounds of the relevant church as listed in the Certificate (the <strong>Church</strong>).</p>
      <ol class="ol1">
        <li class="li4"><strong>observance of terms and variation</strong>
          <ol class="ol1">
            <li class="li4">The Customer agrees to abide by and observe all regulations, restrictions and conditions from time to time laid down by Eternum or as may be amended.</li>
            <li class="li2">The provisions of this agreement (which shall include Schedules 1 and 2 hereto) may be amended from time to time by Eternum by the approval by the board of directors of Eternum of any document amending or superceding this agreement (<strong>Amendments</strong>). Amendments will be effective upon such approval.</li>
            <li class="li2">Amendments shall be made available on the website, <a href="http://www.eternumcolumbarium.com">www.eternumcolumbarium.com</a> (the <strong>Website</strong>). The Customer may request copies of Amendments by notice in writing to Eternum at its registered office. Eternum shall provide copies of the Amendments by post as soon as reasonably practicable and, in any event, within one calendar month from the date of receipt of such request.</li>
            <li class="li2">If at any time the rights to the Columbarium are transferred to the Church or Parish, Eternum will inform the Customer in writing as soon as reasonably practicable. For the avoidance of doubt, the terms and rights under this Agreement, and any subsequent Amendments, shall remain in full force and effect and any reference to Eternum&apos;s obligations shall be amended to refer to the Church or Parish.</li>
          </ol>
        </li>
        <li class="li4"><strong>PERMITTED USE</strong>
          <ol class="ol1">
            <li class="li2">Only the Remains may be placed inside the Niche. No other personal effects may be placed inside the Niche.</li>
            <li class="li2">72 hours&apos; notice to Eternum is required to open the Niche for Inurnment.</li>
            <li class="li2">Inurnment shall take place in course of a religious service at which a religious or lay representative of the relevant parish, as noted on the Certificate (the <strong>Parish</strong>) or Eternum is present, although religious representatives from any other parish may preside at such service with the consent of the priest from the Parish (the <strong>Parish Priest</strong>). In the case of a Deceased Person in respect of whom an Inurnment in accordance with the practice of the Catholic Funeral Rite is requested, this religious service shall refer to the Rite of Committal and shall be presided over by the Parish Priest or any other approved clergy of the Roman Catholic faith.</li>
            <li class="li2">The Parish Priest shall be consulted prior to the performance of any service not performed by approved clergy of the Roman Catholic faith and may, at his sole discretion, disapprove or restrict any proposed ceremony which is not consistent with good taste. Services shall be held with appropriate respect for the Deceased Person as well as others inurned within the Columbarium.</li>
            <li class="li2">Arrangements for Inurnment shall be made through Eternum, who shall consult with the Parish Priest. The date and time of Inurnment shall be set by Eternum in consideration of the requests of the family of the Deceased Person, hours when the Parish and Church will be open and conflicting demands for the Parish, Church and Columbarium.</li>
            <li class="li2">A certificate of cremation (or other acceptable proof) shall accompany the Remains when they are presented for Inurnment, for confirmation of the identity of the Deceased Person.</li>
            <li class="li2">The Remains shall not be divided following Cremation. The Remains shall be treated with the same degree of respect given to a human body and, as such, all of the Remains shall be placed in the Urn and none of the Remains shall be retained for scattering, retention in the home or elsewhere or preservation in mementos or pieces of jewellery or otherwise.</li>
            <li class="li2">The only item that may be placed on the front of the Niche is the brass plaque to be provided by Eternum which shall be inscribed by Eternum with the name, date of birth and date of death of the Deceased Person.</li>
          </ol>
        </li>
        <li class="li4"><strong>MANAGEMENT</strong>
          <ol class="ol1">
            <li class="li4">The Columbarium shall be managed, in its entirety by Eternum. Any queries or requests in relation to the Columbarium, the Niche or Inurnment should be directed to Eternum in the first instance but the Parish will also endeavour to assist where possible.</li>
            <li class="li2">The day-to-day administration and operational tasks in relation to the Columbarium may be delegated by Eternum to any of its employees, contractors or officers.</li>
            <li class="li2">Eternum and its employees, contractors and officers shall be entitled to enforce the bye-laws (the <strong>Bye-Laws</strong>) in relation to the Columbarium (as set out in Schedule 1 of this Agreement, as amended).</li>
          </ol>
        </li>
        <li class="li2"><strong>TRANSFER OF RIGHT</strong>
          <ol class="ol1">
            <li class="li2">The Inurnment Right is non-transferrable under any circumstances. For the avoidance of doubt, alternative arrangements for a Niche may only be made in accordance with this clause 8.&nbsp;</li>
            <li class="li2">The Customer may, by notice in writing, request that:&nbsp;
              <ol class="ol1">
                <li class="li2">Eternum seek an alternative customer for the Niche;&nbsp;</li>
                <li class="li2">if Eternum is able to obtain agreement from any person to purchase a right of inurnment in the Niche, allow the Inurnment Right to lapse; and</li>
                <li class="li2">provide a rebate of such proportion of the Columbarium Fee as shall be determined by Eternum acting reasonably (in consideration of the costs incurred to date) within six weeks of the receipt of funds from the person referred to in clause 8.2.2 above (the <strong>Substituted Customer</strong>).&nbsp;</li>
              </ol>
            </li>
            <li class="li2">Eternum shall have the option to act upon the request referred to in clause 8.2 above shall be under no obligation to do so.</li>
            <li class="li2">The lapse of the Inurnment Right shall not be effective until such time as the Columbarium Fee (which, in this instance, shall have the definition as set out in the agreement to be entered into between Eternum and the Substituted Customer) has been received from the Substituted Customer.</li>
          </ol>
        </li>
        <li class="li4"><strong>closure of the church</strong>
          <ol class="ol1">
            <li class="li4">The Customer acknowledges that the continued management of the Columbarium by Eternum is conditional upon the present church continuing to stand, be owned by the Roman Catholic Church and be utilised as the primary place of worship within the Parish.</li>
            <li class="li2">In the event that the Columbarium requires repair or redevelopment, or if any of the events set out in clauses 9.3 or 9.4 arises, the Customer agrees to the temporary removal of the Remains until such time as those repairs or redevelopments are completed or the Remains are moved to a Replacement Columbarium (as defined below) or Burial Place.</li>
            <li class="li2">In the event that the Church is sold or demolished and, following such sale or demolition, will no longer be utilised as a church and a replacement columbarium is to be constructed within the Parish (<strong>Replacement Columbarium</strong>), Eternum shall, as soon as reasonably practicable but, in any event, by the date being four weeks prior to the date that it is expected that the Church be sold or demolished, contact the Relevant Contacts (in the order set out in the definition of &quot;Relevant Contacts&quot;) using the Contact Information, granting them the option to reserve or use (as the case may be) an inurnment space equivalent to the Niche in the Replacement Columbarium and the remaining provisions of this clause 9.3 shall apply as follows:&nbsp;
              <ol class="ol1">
                <li class="li2">If any of the Relevant Contacts accepts the option referred to in clause 9.3, this agreement shall apply to the equivalent niche in the Replacement Columbarium following the Church being sold or demolished and the terms &quot;Niche&quot; and &quot;Columbarium&quot; shall be construed accordingly.</li>
                <li class="li2">If none of the Relevant Contacts accepts the option referred to in clause 9.3 within two weeks of such option being granted, the Inurnment Right shall lapse as at the date that the Church is sold or demolished and, where such date is after the Inurnment Date, Eternum shall request the Relevant Contacts to remove the Remains within four weeks of such request.</li>
                <li class="li2">If Eternum is unable to contact any of the Relevant Contacts or none of the Relevant Contacts complies with the request referred to in sub-clause 9.3.2, the Inurnment Right shall lapse as at the date that the Church is sold or demolished and the Remains will be removed from the Niche and buried with reverence and in accordance with the practice of the Catholic Funeral Rite (unless, at the time of Inurnment, Eternum is informed in writing that the Remains are not to be buried in accordance with the practice of the Catholic Funeral Rite) in a Burial Place (a record of which shall be maintained by Eternum should it be required by the Deceased Person&apos;s family at any time).</li>
              </ol>
            </li>
            <li class="li2">In the event that the Church is sold or demolished and, following such sale or demolition, will no longer be utilised as a church and no replacement columbarium is to be constructed within the Parish, the Inurnment Right shall lapse as at the date that the Church is sold or demolished and Eternum shall, as soon as reasonably practicable but, in any event, by the date being four weeks prior to the date that it is expected that the Church be sold or demolished, contact the Relevant Contacts (in the order set out in the definition of &quot;Relevant Contacts&quot;) using the Contact Information to notify them accordingly and the remaining provisions of this clause 9.4 shall apply as follows:&nbsp;
              <ol class="ol1">
                <li class="li2">If Eternum is able to contact any of the Relevant Contacts, Eternum shall request such Relevant Contact to remove the Remains within four weeks of such request.</li>
                <li class="li2">If Eternum is unable to contact any of the Relevant Contacts or a Relevant Contact contacted in accordance with clause 9.4 does not comply with the request referred to in sub-clause 9.4.1, the Remains will be removed from the Niche and buried with reverence and in accordance with the practice of the Catholic Funeral Rite (unless, at the time of Inurnment, Eternum is informed in writing that the Remains are not to be buried in accordance with the practice of the Catholic Funeral Rite) in a Burial Place (a record of which shall be maintained by Eternum should it be required by the Deceased Person&apos;s family at any time).</li>
              </ol>
            </li>
            <li class="li2">For the purposes of clause 9.3.3 or 9.4.2, Eternum shall be deemed unable to contact the Relevant Contacts if it has attempted to contact such persons by advertisement in the church bulletin, phone call, post and email using the Contact Information and, as at the date that the Church is sold or demolished, has not received a response. For the avoidance of doubt, Eternum shall be entitled to rely only on the Contact Information and shall not be obliged to carry out any research or investigation for the purposes of contacting the Relevant Contacts should Eternum not have been informed of any changes to the Contact Information.</li>
            <li class="li2">The Customer acknowledges that the closure of the Church is outside of the control of Eternum and that no refund of the Columbarium Fee will be payable in the event of return or burial of Remains.</li>
          </ol>
        </li>
        <li class="li4"><strong>early removal of remains</strong>
          <ol class="ol1">
            <li class="li2">Remains shall only be removed prior to the expiry of the Inurnment Term by an Eligible Relation of the Deceased Person:&nbsp;
              <ol class="ol1">
                <li class="li2">upon receipt by Eternum of written authorisation by the Customer or the Nominated Relation or, where Eternum is unable to contact the Customer<strong>&nbsp;</strong>or the Nominated Relation, written authorisation by any person who is certified by a solicitor to be an Eligible Relation of the Deceased Person;&nbsp;</li>
                <li class="li2">upon provision by such Eligible Relation of a legal document indemnifying Eternum, the Parish and their representatives, employees and officers against all claims, losses, causes of action and associated expenses (including, but not limited to, legal fees and expenses) arising as a result of the removal of the Remains (such document to be provided by Eternum on request); and</li>
                <li class="li2">with the consent of Eternum, such consent not to be unreasonably withheld or delayed where it can be evidenced that the person requesting removal is an Eligible Relation and the documentation required pursuant to clauses 10.1.1 and 10.1.2 above has been provided.&nbsp;</li>
              </ol>
            </li>
            <li class="li2">Upon removal pursuant to clause 10.1, the Inurnment Right shall lapse and no refund of the Columbarium Fee shall be payable.&nbsp;</li>
          </ol>
        </li>
        <li class="li4"><strong>LIMITATION OF LIABILITY</strong>
          <ol class="ol1">
            <li class="li2">Eternum shall not wilfully take any action or wilfully fail to do any act that would result in the loss, destruction or desecration of the Remains or the Niche. However, Eternum accepts no liability for any claim for damages arising as a result of any human act or act of nature or in connection to the use and operation of the Columbarium, save in relation to gross negligence or wilful misconduct on the part of Eternum. In any event, the amount of damages recoverable by the Customer or any persons connected to the Customer shall be limited to the amount of the Columbarium Fee paid. In no event shall Eternum be liable for indirect, special or consequential damages or punitive damages.&nbsp;</li>
            <li class="li2">The Customer is advised to purchase suitable insurance cover to protect against the cost of any repair to or replacement of the Niche or Urn which is damaged or stolen.</li>
          </ol>
        </li>
        <li class="li4"><strong>DATA PROTECTION</strong></li>
      </ol>
      <p class="p5">Eternum processes and stores personal data of the Deceased Person and the Relevant Contacts. In adherence to the requirements of the General Data Protection Regulation, Eternum ensures that such data is held, used and processed in accordance with GDPR and the privacy policy contained at Schedule 2.</p>
      <ol class="ol1">
        <li class="li4"><strong>COMPLIANCE WITH LAW</strong>
          <ol class="ol1">
            <li class="li2">The parties hereto agree to comply with the terms and conditions contained in this agreement and all applicable laws and regulations as in force from time to time.</li>
            <li class="li2">If any provision or part-provision of this agreement is or becomes invalid, illegal or unenforceable, it shall be deemed modified to the minimum extent necessary to make it valid, legal and enforceable. If such modification is not possible, the relevant provision or part-provision shall be deemed deleted. Any modification to or deletion of a provision or part-provision under this clause shall not affect the validity and enforceability of the rest of the agreement.</li>
          </ol>
        </li>
        <li class="li4"><strong>COMPLIANCE WITH CANON LAW</strong>
          <ol class="ol1">
            <li class="li2">All operations and other matters associated with the Columbarium will be conducted in accordance with any pertinent provisions of the Roman Catholic Church&apos;s Code of Canon Law.</li>
            <li class="li2">If any provision or part-provision of this agreement is or becomes incompatible with the Roman Catholic Church&apos;s Code of Canon Law, it shall be deemed modified to the minimum extent necessary to make it compatible. If such modification is not possible, the relevant provision or part-provision shall be deemed deleted. Any modification to or deletion of a provision or part-provision under this clause shall not affect the validity and enforceability of the rest of the agreement.</li>
          </ol>
        </li>
        <li class="li4"><strong>notices</strong>
          <ol class="ol1">
            <li class="li2">Any notice or other communication given to a party under or in connection with this agreement shall be in writing, addressed to that party at (in the case of Eternum) its registered office or (in the case of the Customer or Next of Kin) such other address as has been notified to Eternum in writing in accordance with this clause, and shall be delivered personally, or sent by pre-paid first class post or other next working day delivery service, commercial courier, or email.</li>
            <li class="li2">A notice or other communication shall be deemed to have been received: if delivered personally, when left at the address referred to in clause 15.1; if sent by pre-paid first class post or other next working day delivery service, at 9.00 am on the second business day (being a day (other than a Saturday, Sunday or public holiday) when banks in Belfast are open for business) (a <strong>Business Day</strong>) after posting; if delivered by commercial courier, on the date and at the time that the courier&apos;s delivery receipt is signed; or, if sent by email, one Business Day after transmission.</li>
            <li class="li2">The provisions of this clause shall not apply to the service of any proceedings or other documents in any legal action.</li>
          </ol>
        </li>
        <li class="li4"><strong>GOVERNING LAW AND JURISDICTION</strong>
          <ol class="ol1">
            <li class="li2"><strong>Governing law.&nbsp;</strong>This agreement, and any dispute or claim (including non-contractual disputes or claims) arising out of or in connection with it or its subject matter or formation, shall be governed by, and construed in accordance with the law of the Republic of Ireland.</li>
            <li class="li2"><strong>Jurisdiction.</strong> Each party irrevocably agrees that the courts of the Republic of Ireland shall have exclusive jurisdiction to settle any dispute or claim (including non-contractual disputes or claims) arising out of or in connection with this agreement or its subject matter or formation.&nbsp;</li>
          </ol>
        </li>
      </ol>
      <p class="p2"><b>A copy of these terms and conditions will be sent to you along with a prepaid envelope. The document must be signed and returned to Eternum.</b></p>
      <ol class="ol1">
        <li class="li12"><strong>&nbsp; &nbsp; 1.</strong><strong>: BYE-LAWS OF THE COLUMBARIUM</strong></li>
        <li class="li2">Save as set out in paragraph 2, the Columbarium shall be open at the following times (which may be subject to change from time to time depending on when the Church is open):</li>
      </ol>
      <p class="p1">[INSERT OPENING DAYS AND TIMES]</p>
      <ol class="ol1">
        <li class="li2">[INSERT DETAILS OF EXCEPTIONS TO STANDARD OPENING TIMES, E.G. RELIGIOUS HOLIDAYS]</li>
        <li class="li2">The annual mass and blessing of graves within the Parish shall extend also to the Columbarium.</li>
        <li class="li2">During the times set out in paragraph 1 above (subject to the exceptions contained paragraph 2 above), friends and family will be able to visit and spend time in the Columbarium in quiet reflection.&nbsp;</li>
        <li class="li2">All visitors shall act with the decorum normally observed in a sacred area of a church and with the appropriate respect for those inurned within the Columbarium.&nbsp;</li>
        <li class="li2">Any representative of Eternum and/or any religious or lay representative of the Parish shall be entitled to exclude any persons who do not act with the appropriate degree of respect.</li>
        <li class="li2">Due to health and safety concerns, floral tributes are not permitted in the Columbarium.</li>
        <li class="li2">Eternum and its employees, contractors or officers shall not be responsible for any decorations, flowers or other items placed in the Columbarium.</li>
        <li class="li2">Eternum reserves the right to remove and dispose of:
          <ol class="ol1">
            <li class="li2">any item that is deemed to be too large;</li>
            <li class="li2">any glass or ceramic containers or other item that is deemed to pose a health and safety risk or capable of causing injury;</li>
            <li class="li2">any item that is deemed to be unsightly or objectionable;</li>
            <li class="li2">any item that contains or displays political or otherwise offensive symbols or writing;</li>
            <li class="li2">any item that contains or displays symbols or writing that are deemed to be inimical to the values of the Roman Catholic faith;</li>
            <li class="li2">any flowers or other plants that have perished.</li>
          </ol>
        </li>
        <li class="li2">The removal of any items pursuant paragraph 9 shall be at the sole discretion of Eternum.&nbsp;</li>
      </ol>
      <ol class="ol1">
        <li class="li15"><strong>&nbsp; &nbsp; 2.</strong><strong>: privacy policy</strong></li>
      </ol>
      <p class="p16">Eternum (N.I.) Ltd. (<strong>Eternum</strong>) is a company that supplies and maintains columbarium facilities in the UK &amp; Ireland registered as a company in Northern Ireland. For the purposes of this privacy policy, the terms &ldquo;we&rdquo; and &quot;our&quot;&nbsp;refers to Eternum.</p>
      <p class="p16">We process and store personal information relating to our customers. In adherence to the requirements of the General Data Protection Regulation (<strong>GDPR</strong>) we ensure the personal information we obtain is held, used, and processed in accordance with GDPR.&nbsp;</p>
      <p class="p16">We promise to respect any personal data of the Deceased Person and the Relevant Contacts shared with us and keep it safe. &nbsp;We will be clear when we collect data about why we are collecting it and what we intend to do with it, and not do anything that any person dealing with us wouldn&rsquo;t reasonably expect.&nbsp;</p>
      <p class="p16"><strong>Acceptance of this notice</strong></p>
      <p class="p16">By entering into this agreement and providing personal information, the Customer giving consent to our collection and use of the information provided in the ways set out in this notice. We will, from time to time, review and update this privacy policy and will update, modify, add or remove sections at our discretion. &nbsp;If this happens, we will post the changes on the Website and they will apply from the time that they are approved by the board of Eternum. A hard copy of the privacy policy from time to time can be provided, by post, on request.</p>
      <p class="p16">Your continued acceptance of the agreement and/or the continued provision of personal information after we have posted the changes to these terms will be taken to mean that the Customer is in agreement with those changes.&nbsp;</p>
      <p class="p16"><strong>What information do we collect?</strong></p>
      <p class="p16">We collect personal information when a person enquires about our services and products, enters into agreements with us for the provision of our products and services, provide or update Relevant Contact contact details, provide us with details of the Deceased Person for Inurnment and inscription of the plaque to be affixed to the Niche, or otherwise give us personal information.&nbsp;We will only collect the data we need for the purposes we specify at the time of collection and will not use this data for any purpose different form the one specified at collection.</p>
      <p class="p16">Personal information can include your name, date of birth, email address, postal address, telephone number, mobile telephone number, fax number, bank account details and credit/debit card details. In short, any information that can identify you. We will, on a regular basis, provide to the Parish, a register of persons interred in the Columbarium.</p>
      <p class="p16"><strong>Why we collect your information</strong></p>
      <p class="p16">We may collect your personal information for a number of reasons, such as:</p>
      <ul class="ul1">
        <li class="li16">to provide you with the services, products or information you have requested</li>
        <li class="li16">for administration purposes e.g. we may contact you about any matters affecting the Niche and this agreement;</li>
        <li class="li16">for internal record keeping, such as the management of feedback or complaints;&nbsp;</li>
        <li class="li16">to analyse and improve the services we offer;&nbsp;</li>
        <li class="li16">where it is required or authorised by law.</li>
      </ul>
      <p class="p16"><strong>How we use your information</strong></p>
      <p class="p16">We would like to contact you in respect of any matters affecting the Niche and this agreement. In some cases, we will rely on our legitimate interests to contact you where those interests are in line with what we think your reasonable expectations might be, for instance where any term referred to in this agreement is due to expire or where temporary or permanent removal of Remains is required.</p>
      <p class="p16">We will not sell or swap your information with any third party other than the Parish. The Parish is required to comply with data protection laws.</p>
      <p class="p16">We may disclose your personal information to third parties if we are required to do so through a legal obligation (for example to the police or a government body); to enable us to enforce or apply our terms and conditions or rights under an agreement; or to protect us, for example, in the case of suspected fraud or defamation.&nbsp;</p>
      <p class="p16">We aim to ensure that all information we hold about you is accurate and, where applicable, kept up to date. If any of the information we hold about you is inaccurate and if you advise us, we will ensure it is amended.&nbsp;</p>
      <p class="p16">We will only keep your information for as long as required to enable us to carry out the purpose for which it was collected. We will not keep your information for any longer than is necessary. When we no longer need to retain your information we will ensure it is securely disposed of, at the appropriate time.&nbsp;We do not transfer your personal data outside the UK and Ireland.&nbsp;</p>
      <p class="p16"><strong>Our website</strong></p>
      <p class="p16">Our website has its own privacy policy which should be reviewed should you wish to use the website.</p>
      <p class="p16"><strong>Your rights</strong></p>
      <p class="p16">You can choose the type of communications and information you receive about Eternum. You can change your mind at any time by contacting <a href="mailto:info@eternumcolumbarium.com">info@eternumcolumbarium.com</a></p>
      <p class="p16">You have the right to:</p>
      <ul class="ul1">
        <li class="li16">request a copy of the information we hold about you;</li>
        <li class="li16">update or amend the information we hold about you if it is wrong;</li>
        <li class="li16">change your communication preferences at any time;</li>
        <li class="li16">ask us to remove your personal information from our records;</li>
        <li class="li16">object to the processing of your information for marketing purposes; or</li>
        <li class="li16">raise a concern or complaint about the way in which your information is being used.</li>
      </ul>
      <p class="p16">If you have questions or queries about this privacy, to obtain a copy of the information we hold about you or to make a complaint about how your personal data has been used by us, please contact our Data Protection Officer at:</p>
      <p class="p16">Data Protection Officer</p>
      <p class="p16">Eternum (N.I.) Ltd</p>
      <p class="p20">Tel: 02895 582 550<br> Email: <a href="mailto:info@eternumcolumbarium.com">info@eternumcolumbarium.com</a></p>
      <p class="p22">You will not have to pay a fee to access your personal data (or to exercise any of the other rights). However, we may charge a reasonable fee if your request is clearly unfounded, repetitive or excessive. Alternatively, we could refuse to comply with your request in these circumstances.</p>
      <p class="p24">We may need to request specific information from you to help us confirm your identity and ensure your right to access your personal data (or to exercise any of your other rights). This is a security measure to ensure that personal data is not disclosed to any person who has no right to receive it. We may also contact you to ask you for further information in relation to your request to speed up our response. We try to respond to all legitimate requests within one month. Occasionally it could take us longer than a month if your request is particularly complex or you have made a number of requests. In this case, we will notify you and keep you updated.</p>
      <p class="p22">You have the right to make a complaint at any time to the regulator in the country you are based. In the UK this is the Information Commissioner&apos;s Office (ICO), the (<a href="http://www.ico.org.uk/">www.ico.org.uk</a>) and in Ireland this is the Data Protection Commissioner (DPC) (<a href="https://www.dataprotection.ie/">https://www.dataprotection.ie/</a>).We would, however, appreciate the chance to deal with your concerns before you approach the ICO or DPC so please contact us in the first instance.</p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection