@php
$page = 'Locations';
$pagetitle = 'Locations - Eternum Columbarium';
$metadescription = 'See our locations';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 text-center">
      <h1 class="mt-5 mob-mt-0 faq-title">Locations</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
    </div>
  </div>
</header>
@endsection
@section('content')
<locations-index></locations-index>
<div class="container-fluid py-5 bg-light">
	<div class="row py-5 text-center">
		<div class="col-12">
			<h3>Don't see the location you are looking for?</h3>
			<p>Please get in touch and we will get back to you when your preferred location becomes available.</p>
			<a href="/contact">
				<button type="button" class="btn btn-primary btn-icon">Contact us <i class="fa fa-chevron-right"></i></button>
			</a>
		</div>
	</div>
</div>
<footer-top></footer-top>
@endsection