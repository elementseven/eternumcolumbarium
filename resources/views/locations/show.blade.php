@php
$page = 'Location';
$pagetitle = $partner->title . ' | Eternum Columbarium';
$metadescription = $partner->excerpt;
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://eternumcolumbarium.com' . $partner->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container position-relative py-5 mob-pb-0">
  <div class="row pt-5 mob-mb-5">
    
    <div class="col-lg-6 text-center text-lg-left">
      <p class="pt-5 mt-5 mob-pt-0 mb-1"><b><a href="{{route('locations')}}"><i class="fa fa-arrow-circle-left"></i>&nbsp; Browse locations</a></b></p>
      <h1 class="blog-title mb-4">{{$partner->title}}</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic"/>
      <br>
      <a class="text-larger" href="{{$partner->link}}" target="_blank">
        <div class="btn btn-primary btn-small mt-4">Visit Website</div>
      </a> 
    </div>
    <div class="col-lg-6 pt-5 mob-pt-3 pr-5 mob-px-1">
      <div class="rounded-image mt-5 mob-px-4">
        <picture> 
          <source media="(min-width: 900px)" srcset="{{$partner->getFirstMediaUrl('partners', 'normal')}} 1x, {{$partner->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <source media="(min-width: 601px)" srcset="{{$partner->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$partner->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <source srcset="{{$partner->getFirstMediaUrl('partners', 'normal-webp')}} 1x, {{$partner->getFirstMediaUrl('partners', 'double-webp')}} 2x" type="image/webp"/> 
          <img srcset="{{$partner->getFirstMediaUrl('partners', 'normal')}} 600w, {{$partner->getFirstMediaUrl('partners', 'double')}} 900w, {{$partner->getFirstMediaUrl('partners', 'double')}} 1440w" src="{{$partner->getFirstMediaUrl('partners', 'double')}}" type="{{$partner->getFirstMedia('partners')->mime_type}}" alt="{{$partner->title}}" class="w-100" />
        </picture>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row text-center text-lg-left">
        <div class="col-xl-10 mob-mt-0 blog-body">
          {!!$partner->body!!}
          @if($partner->second_title && $partner->second_excerpt && $partner->second_content)
          <h3 class="mt-5">{{$partner->second_title}}</h3>
          <p >{{$partner->second_excerpt}}...</p>
          <div class="collapse" id="location-second">{!!$partner->second_content!!}</div>
          <p>
            <button class="btn btn-primary btn-small mt-3" type="button" data-toggle="collapse" data-target="#location-second" aria-expanded="false" aria-controls="location-second">
              Read More
            </button>
          </p>
          @endif
        </div>
        <div class="col-12 my-5 ">
          <p class="mb-1 text-larger"><b>Share this location:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($partner->title)}}&amp;summary={{urlencode($partner->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($partner->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($partner->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
@if($partner->id ==  4)
<div class="container-fluid bg-light">
  <div class="row">
    <div class="container py-5 mb-5 mob-mb-0">
      <div class="row text-center text-lg-left">
        <div class="col-lg-12 text-center">
              <h2 class="mt-5 mob-mt-0 faq-title">Select a niche</h2>
              <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3 pb-4" />
            </div>
        <div class="col-lg-8 pr-5 mob-px-3 text-left">
          <img src="/storage/{{$partner->diagram}}" alt="{{$partner->title}} columbarium diagram" class="w-100"/>
          <p class="mt-3 text-small">If you have any questions about selecting a niche or position, <a href="/contact">please get in touch</a> and we will be happy to assist.</p>
        </div>
        <div class="col-lg-4">
          <p>Our columbarium is now fully reserved. The diocese is discerning the possibility of having another vault built in the columbarium chapel. If you would like to add your name to the list of interested people, please <a href="/contact">contact us</a>.</p>
          {{-- <select-position :partner="{{$partner}}" :lease="{{$partner->leases[0]->id}}"></select-position> --}}
        </div>
      </div>
    </div>
  </div>
</div>
@endif
<footer-top></footer-top>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection