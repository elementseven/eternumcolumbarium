@php
$page = 'Login';
$pagetitle = 'Login - Eternum Columbarium';
$metadescription = 'Login to Eternum Columbarium';
$pagetype = 'light';
$pagename = 'login';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid text-center">
    <div class="container">
        <div class="row pt-5 justify-content-center">
            
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container my-5 py-5">
  <div class="row pt-5 justify-content-center">
    <div class="col-lg-4">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
          <h1 class="page-top text-center mb-4 scroll-fade mw-100" data-fade="fadeIn">Login</h1>
          <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group row">
              <div class="col-md-12">
                <label for="email" class="mb-0">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="password" class="mb-0">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12 text-center">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label class="form-check-label pl-0" for="remember"><b>{{ __('Remember Me') }}</b></label>
                </div>
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary mx-auto">{{ __('Login') }}</button>
                @if (Route::has('password.request'))
                <a class="pt-2 d-block text-center text-dark" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                @endif
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  
</div>
@endsection