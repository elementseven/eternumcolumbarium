@php
$page = 'Password Reset';
$pagetitle = 'Password Reset - Fibrus';
$metadescription = 'Reset your password for Fibrus';
$pagetype = 'light';
$pagename = 'login';
$ogimage = 'https://fibrus.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid text-center">
    <div class="container">
        <div class="row pt-5 justify-content-center">
            
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container py-5 my-5">
    <div class="row justify-content-center mob-pt-0">
        <div class="col-lg-6 text-center">
            <h1 class="my-3">Password Reset</h1>
            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">
                
                <div class="form-group row">
                    

                    <div class="col-md-12 text-left">
                        <label for="email" class="pl-3"><b>{{ __('E-Mail Address') }}</b></label>
                        <input id="email" type="email" placeholder="Email Address" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    

                    <div class="col-md-12 text-left">
                        <label for="password" class="pl-3"><b>New Password</b></label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="New Password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12 text-left">
                        <label for="password-confirm" class="pl-3"><b>{{ __('Confirm New Password') }}</b></label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm New Password">
                    </div>
                </div>

                <div class="form-group row mb-0 mt-4 text-center">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
