@php
$page = 'Password Reset';
$pagetitle = 'Password Reset - Fibrus';
$metadescription = 'Reset your password for Fibrus';
$pagetype = 'light';
$pagename = 'login';
$ogimage = 'https://fibrus.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid text-center">
    <div class="container">
        <div class="row pt-5 justify-content-center">
            
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container py-5 my-5">
    <div class="row justify-content-center pt-5 mob-pt-0">
        <div class="col-lg-6 text-center">
            <h1 class="my-3">Forgot Password</h1>
            <p class="mb-5 text-large">Enter your email address and we will send you a password reset link.</p>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="form-group row">
                    

                    <div class="col-md-12 text-left ">
                        <label for="email" class="pl-3"><b>{{ __('E-Mail Address') }}</b></label>
                        <input id="email" type="email" placeholder="Email Address" class="form-control text-left @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Send Reset Link') }}
                        </button>
                    </div>
                </div>
            </form>
            @if (session('status'))
                <div class="alert alert-success mt-4" role="alert">
                    {{ session('status') }}
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
