@php
$page = 'News';
$pagetitle = $memorial->title . ' | Eternum Columbarium';
$metadescription = $memorial->excerpt;
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://eternumcolumbarium.com' . $memorial->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 mob-mb-5 mob-pt-0 mob-mb-0">

    <div class="col-lg-4 pt-5 mob-pt-0">
      <img src="{{$memorial->getFirstMediaUrl('memorials')}}" class="w-100 my-5 mob-mb-3" alt="{{$memorial->name}} blogs image"/>
    </div>
    <div class="col-lg-8 text-center text-lg-left">
      <p class="pt-5 mt-5 mob-pt-0 mob-mt-0 mb-1"><b><a href="{{route('memorials')}}"><i class="fa fa-arrow-circle-left"></i>&nbsp; Browse memorials</a></b></p>
      <h1 class="memorial-title mb-4 mob-mb-3">{{$memorial->title}}</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="mb-4 mob-mb-3" />
      @if($memorial->birth && $memorial->death)
        <p class="text-large mb-1">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $memorial->birth)->format('d/m/Y')}} - {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $memorial->death)->format('d/m/Y')}}</p>
      @endif
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 mob-pt-0 text-center text-lg-left">
        <div class="col-xl-10 mob-mt-0 blog-body">
          {!!$memorial->body!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1 text-larger"><b>Share this memorial:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($memorial->title)}}&amp;summary={{urlencode($memorial->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($memorial->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($memorial->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<footer-top></footer-top>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection