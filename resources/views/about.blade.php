@php
$page = 'About';
$pagetitle = 'About Eternum Columbarium | Columbarium Solutions for Ireland & The UK';
$metadescription = '';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 text-center">
      <h1 class="mt-5 mob-mt-0 faq-title">About Us</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="container pb-5 mob-pb-0 mob-px-4">
      <div class="row justify-content-center">
        <div class="col-lg-10">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100 text-center">
              <p class="text-larger mb-4">Eternum’s hand crafted columbaria address the shortage of burial space in Ireland and the UK. As the availability of graves decreases, our beautiful, bespoke columbaria give you a new option for burial in iconic churches and locations. This makes visiting the burial site of loved ones a much more comfortable and meaningful experience.</p>
              <a href="{{route('locations')}}">
                <div class="btn btn-primary btn-icon">View Locations <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/695765017?h=5a55cdf4e8" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid mb-5">
  <div class="row">
    <div class="container py-5 mt-5 mob-pb-0 mob-px-4">
      <div class="row text-center text-lg-left">
        <div class="col-lg-8">
          <p class="text-large">Eternum was founded to help address the critical shortage of burial space in Ireland and the UK, with many people now finding it almost impossible to find a grave. Eternum recognises that this adds a great deal of stress at an already very difficult time and offers you a beautiful solution. Our columbaria provide the perfect resting place for the ashes of your loved ones.</p>
          <p>Rather than be kept at home or scattered, internment in a columbarium is a fitting way to lay someone’s earthly remains to rest. Eternum’s relationships with churches and iconic locations offers you the peace of mind that your loved one will rest in a secure, spiritual and dignified space.</p>
          <p>Internment in a columbarium makes it possible for people who live abroad to be buried at home. Repatriation of a body is prohibitively expensive for most people, but this is not the case when repatriating ashes.</p>
          <p class="mb-4">All of our columbaria offer prayerful and dignified spaces for quiet reflection and joyful recollection. </p>
        </div>
        <div class="col-lg-4 mob-px-5">
          <img src="/img/temp/logo-symbol.png" class="w-100" alt="Urn temporay image"/>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid bg1 bg bg-fixed py-5 position-relative">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container py-5 text-white text-center">
      <div class="row">
        <div class="col-lg-4">
          <img src="/img/shapes/church.svg" alt="Columbariums in church Northern Ireland" height="80" class="mb-3" />
          <h4>LOCAL</h4>
          <p class="text-large mb-0">Rest in a familiar church</p>
        </div>
        <div class="col-lg-4 mob-mt-5">
          <img src="/img/shapes/cross.svg" alt="Cross in church Northern Ireland" height="80" class="mb-3" />
          <h4>SACRED</h4>
          <p class="text-large mb-0">At the centre of faith</p>
        </div>
        <div class="col-lg-4 mob-mt-5">
          <img src="/img/shapes/family.svg" alt="Family in church Northern Ireland" height="80" class="mb-3" />
          <h4>SECURE</h4>
          <p class="text-large mb-0">Comfort for you and your loved ones</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="container py-5 my-5 mob-pb-0 mob-px-4">
      <div class="row text-center text-lg-left">
        <div class="col-lg-10">
          <h2>Our Services</h2>
          <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
          <p class="text-large">We understand that choosing a resting place for a loved one is a sensitive decision and one that can sometimes be made in a time of unbearable grief. Ashes are the earthly remains of a person who was loved by their family and celebrated in their community. As the body of a loved one is treated with dignity, reverence and respect before cremation, so too are the ashes interred in an Eternum Columbarium.</p>
          <p>Our website makes it very easy to view our locations, select a niche and buy a lease. You can see the churches where our columbaria are located, read about their history and choose the niche you want. You can also visit any of our columbaria during the opening hours of the church to experience the peaceful atmosphere.</p>
          <p class="mb-4">If you would like to reserve a number of niches or a family plot, please get in touch with us and a member of staff will be happy to assist.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<footer-top></footer-top>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection