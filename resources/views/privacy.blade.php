@php
$page = 'Privacy Policy';
$pagetitle = 'Privacy Policy | Eternum Columbarium';
$metadescription = 'Columbarium Solutions for Ireland & The UK';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 text-center text-lg-left">
    <div class="col-lg-12 mt-5">
      <h1 class="mt-5 mob-mt-0 faq-title">Privacy Policy</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3 mb-5" />
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mob-pb-0">
  <div class="row text-center text-lg-left">
    <div class="col-12">
      <p>Eternum (N.I.) Ltd. (Eternum) is a company that supplies and maintains columbarium facilities in the UK & Ireland registered as a company in Northern Ireland.</p>
      <p>For the purposes of this notice, the terms “we” and "our" refers to Eternum.</p>
      <p>We process and store personal information relating to our customers. In adherence to the requirements of the General Data Protection Regulation (GDPR) we ensure the personal information we obtain is held, used, and processed in accordance with the Regulation. </p>
      <p>We promise to respect any personal data you share with us and keep it safe.  We will be clear when we collect your data about why we are collecting it and what we intend to do with it, and not do anything you wouldn’t reasonably expect. </p>
      <h3 class="mb-3 mt-5">Your acceptance of this notice</h3>
      <p>By using our websites and social media pages or providing your personal information, you are giving your consent to our collection and use of the information you provide in the ways set out in this notice. We will, from time to time, review and update this Privacy Notice and will update, modify, add or remove sections at our discretion.  If this happens, we will post the changes on this page and they will apply from the time we post them. Your continued use of our website, any of our services and/or the continued provision of personal information after we have posted the changes to these terms will be taken to mean you are in agreement with those changes. This notice was last changed on 25th May 2018.</p>
      <h3 class="mb-3 mt-5">What information do we collect?</h3>
      <p>We collect personal information about you when you enquire about our services and products, purchase our products, when you engage with us on social media, or otherwise give us personal information. We will only collect the data we need for the purposes we specify at the time of collection and will not use this data for any purpose different from the one specified at collection.</p>
      <p>Personal information can include your name, date of birth, email address, postal address, telephone number, mobile telephone number, fax number, bank account details and credit/debit card details. In short, any information that can identify you.</p>
      <p>If you read pages or download information from our website, we may gather information about this use, such as which pages are most visited.  This information can be used to help us improve our website and services and ensure we provide you with the best service.  Wherever possible, the information we use for this purpose will be anonymised.  More information can be found in the section below entitled “Our website”.</p> 
      <h3 class="mb-3 mt-5">Why we collect your information</h3>
      <p>We may collect your personal information for a number of reasons, such as:</p>
      <ul class="text-left">
        <li><p>to provide you with the services, products or information you have requested</p></li>
        <li><p>for administration purposes e.g. we may contact you about an enquiry you’ve made or with a receipt for a product you have purchased</p></li>
        <li><p>for internal record keeping, such as the management of feedback or complaints; </p></li>
        <li><p>to analyse and improve the services we offer; </p></li>
        <li><p>where it is required or authorised by law.</p></li>
      </ul>
      <h3 class="mb-3 mt-5">How we use your information</h3>
      <p>We would like to contact you with information about our products and services by mail, e-mail, telephone, mobile telephone or social messaging. Most of the time we will be relying on your explicit consent to process your personal information. In some cases, we will rely on our legitimate interests to contact you where those interests are in line with what we think your reasonable expectations might be, for instance where you have been in touch with us within the last two years.</p>
      <p>We will not sell or swap your information with any third party. We may share your information with a partner organisation because of the nature of our work. Our trusted partners are required to comply with data protection laws and our high standards and are only allowed to process your information in strict compliance with our instructions.</p>
      <p>We may disclose your personal information to third parties if we are required to do so through a legal obligation (for example to the police or a government body); to enable us to enforce or apply our terms and conditions or rights under an agreement; or to protect us, for example, in the case of suspected fraud or defamation. </p>
      <p>We aim to ensure that all information we hold about you is accurate and, where applicable, kept up to date. If any of the information we hold about you is inaccurate and either you advise us or we become otherwise aware, we will ensure it is amended. </p>
      <p>We will only keep your information for as long as required to enable us to carry out the purpose for which it was collected. We will not keep your information for any longer than is necessary. When we no longer need to retain your information we will ensure it is securely disposed of, at the appropriate time in line with our data retention policy. </p>
      <h3 class="mb-3 mt-5">Our website</h3>
      <p>Our website use cookies to help it work well and to track information about how people are using it. A cookie is a small file of letters and numbers that we may put on your computer or mobile device when you access our website.  These cookies allow us to distinguish you from other users of the website helping us to provide you with a good experience when you browse our website and also to improve our site. For example, they will tell us whether you have visited our site before or whether you are a new visitor.</p>
      <p>For all areas of our website which collect personal information, we use a secure server. We enforce strict procedures and security features to protect your information and prevent unauthorised access. Our website contains links to other websites belonging to third parties and we sometimes choose to participate in social networking sites including but not limited to Facebook, Twitter, YouTube, Instagram and LinkedIn. We may include content from sites such as these on our websites but we do not have any control over the privacy practices of these other sites. You should make sure when you leave our site that you have read and understood that site’s privacy policy in addition to our own.</p>
      <h3 class="mb-3 mt-5">Your rights</h3>
      <p>You can choose  the type of communications and information you receive about Eternum. You can change your mind at any time by contacting info@eternumcolumbarium.com</p>
      <p>You have the right to:</p>
      <ul class="text-left">
        <li><p>request a copy of the information we hold about you;</p></li>
        <li><p>update or amend the information we hold about you if it is wrong;</p></li>
        <li><p>change your communication preferences at any time;</p></li>
        <li><p>ask us to remove your personal information from our records;</p></li>
        <li><p>object to the processing of your information for marketing purposes; or</p></li>
        <li><p>raise a concern or complaint about the way in which your information is being used.</p></li>
      </ul>
      <p class="mt-5">If you have questions or queries about this Privacy Notice, to obtain a copy of the information we hold about you or to make a complaint about how your personal data has been used by us, please contact our Data Protection Officer at:</p>
      <p class="mb-5">Data Protection Officer<br>Eternum (N.I.) Ltd<br>Tel: 02895 582 550<br>Email: info@eternumcolumbarium.com</p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection