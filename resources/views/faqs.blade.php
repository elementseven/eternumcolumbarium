@php
$page = 'FAQs';
$pagetitle = 'FAQs | Eternum Columbarium';
$metadescription = 'Columbarium Solutions for Ireland & The UK';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 text-center">
      <h1 class="mt-5 mob-mt-0 faq-title">FAQs</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mob-pb-0">
  <div class="row">
    <faqs></faqs>
  </div>
</div>
<footer-top></footer-top>
@endsection
@section('scripts')
@endsection