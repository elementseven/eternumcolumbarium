@php
$page = 'Partnerships';
$pagetitle = 'Partnerships - Eternum Columbarium | Columbarium Solutions for Ireland & The UK';
$metadescription = '';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 text-center">
      <h1 class="mt-5 mob-mt-0 faq-title">Partnerships</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="container py-5 my-5 mob-pb-0 mob-px-4">
      <div class="row text-center text-lg-left">
        <div class="col-lg-8">
          <p class="text-large">Eternum was founded by Joseph O’Neill who has many years of experience working with churches and other institutional bodies. Working with the finest artists and craftspeople in Ireland, Eternum has created columbaria that are liturgical works of art and that fit perfectly in parish estates.</p>
          <p>Eternum offers a full service columbarium management plan to parishes, organisations and institutions across Ireland and the UK. As burial space becomes ever more scarce and the demand for cremation increases, having a columbarium is an excellent way to provide a much needed service whilst earning a vital income.</p>
          <p>Having a columbarium connected to your parish/organisation brings many benefits. It fulfils one of the corporal works of mercy, burying the dead, as well as providing your community and others from the surrounding area with a beautiful resting place. As graveyards fill up and alternatives are sourced further from our towns and cities, having a columbarium in your property serves an increasingly unmet need in our society. This is a wonderful way for the parish/organisation to preserve a sacred building, utilise space and earn an income.</p>
          <p class="mb-4">A member of Eternum staff will be happy to talk about our services and how we can assist your parish/organisation develop and maintain a beautiful columbarium.</p>
        </div>
        <div class="col-lg-4 pl-5 mob-px-5 mob-py-5">
          <img srcset="/img/shapes/celtic-cross.svg" type="image/svg" alt="Celtic cross icon Eternum Columbarium" class="w-100" />
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/695766285?h=c2e621cb04" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid bg1 bg bg-fixed py-5 position-relative">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container py-5 text-white text-center">
      <div class="row">
        <div class="col-lg-4">
          <img src="/img/shapes/church.svg" alt="Columbariums in church Northern Ireland" height="80" class="mb-3" />
          <h4>LOCAL</h4>
          <p class="text-large mb-0">Rest in a familiar church</p>
        </div>
        <div class="col-lg-4 mob-mt-5">
          <img src="/img/shapes/cross.svg" alt="Cross in church Northern Ireland" height="80" class="mb-3" />
          <h4>SACRED</h4>
          <p class="text-large mb-0">At the centre of faith</p>
        </div>
        <div class="col-lg-4 mob-mt-5">
          <img src="/img/shapes/family.svg" alt="Family in church Northern Ireland" height="80" class="mb-3" />
          <h4>SECURE</h4>
          <p class="text-large mb-0">Comfort for you and your loved ones</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="container py-5 my-5 mob-pb-0 mob-px-4">
      <div class="row text-center text-lg-left">
        <div class="col-lg-10">
          <h2>Interested in a Columbarium?</h2>
          <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
        </div>
        <div class="col-lg-10">
          <p>Send us your details using the form below and we will be in touch.</p>
          <partnership-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></partnership-form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0 flowerbg bg position-relative">
  <div class="trans"></div>
  <div class="row py-5 justify-content-center text-center text-lg-left">
    <div class="container py-5">
      <div class="row">
        <div class="col-lg-10 text-white">
          <h2 class="mb-4 smaller">View our beautiful columbaria</h2>
          <a href="{{route('locations')}}">
            <div class="btn btn-primary btn-icon">View locations <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection