<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#000000">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<title>{{$pagetitle}}</title>

	<meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Eternum Columbarium">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:description" content="{{$metadescription}}">
{{--   <style>@import url(https://fonts.googleapis.com/css2?family=Lora:wght@700&family=Nunito+Sans:wght@400;700&display=swap);</style>
 --}}  <meta name="description" content="{{$metadescription}}">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",  	    
    "telephone": "+442890993230",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5XLS8DB');</script>
<!-- End Google Tag Manager -->
</head>
<body class="front">
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5XLS8DB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	@yield('fbroot')
	<div id="main-wrapper">
		<div id="app" class="front {{$pagetype}}">
			<div id="menu_btn" class="menu_btn float-left d-lg-none"><div class="nav-icon"><span></span><span></span><span></span></div></div>
			<div id="main-menu" class="menu">
        <div id="main-menu-top" class="container-fluid px-5 ipad-px-3 mob-px-1 bg-primary overflow-hidden text-white d-none d-lg-block">
          <div class="row">
            <div class="col-lg-10">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="mailto:info@eternumcolumbarium.com" class="text-white"><i class="fa fa-envelope mr-1"></i> info@eternumcolumbarium.com</a>
                <a href="tel:00447821451233" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +44 (0) 7821 451233</a>
                <a href="tel:00353852148301" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +353 (0) 852 148301</a>
              </p>
            </div>
            <div class="col-lg-2 text-center text-lg-right mob-px-0 d-none d-lg-block">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="https://www.facebook.com/EternumColumbarium/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-facebook ml-4"></i></a>
                <a href="https://www.instagram.com/eternumcolumbarium/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/EternumNILtd" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
				<div class="container-fluid px-5 ipad-px-3 mob-px-3">
					<div class="row">
						<div class="col-lg-2 mob-mt-2 py-2">
							<a href="/">
                @if($pagetype == 'light')
                <img src="/img/logos/logo.svg" class="menu_logo" alt="Eternum Columbarium Logo" width="150" />
                @else
								<img src="/img/logos/logo-white.svg" class="menu_logo" alt="Eternum Columbarium Logo" width="150" />
                @endif
							</a>
						</div>
						@if($page != '404')
						<div class="col-lg-10 text-right d-none d-lg-block mt-4">
							<div class="menu-links d-inline-block">
								<a href="{{route('welcome')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Home</a>
							</div>
							<div class="menu-links d-inline-block ">
								<a href="{{route('about')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">About</a>
							</div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('partnerships')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Partnerships</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('locations')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Locations</a>
              </div>
              {{-- <div class="menu-links d-inline-block ">
                <a href="{{route('memorials')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Memorials</a>
              </div> --}}
              <div class="menu-links d-inline-block ">
                <a href="{{route('news')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">News</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('faqs')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">FAQs</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('contact')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Contact</a>
              </div>
              @if(Cart::count())
              <div class="menu-links d-inline-block ml-3">
                <a href="{{route('basket')}}">
                  <span class="basket-count"><i class="fa fa-shopping-cart"></i><span>{{Cart::count()}}</span></span>
                </a>
              </div>
              @endif
            </div>
            @endif
          </div>
        </div>
      </div>
      <div id="scroll-menu" class="menu d-none d-lg-block">
        <div id="main-menu-top" class="container-fluid px-5 ipad-px-3 mob-px-1 bg-primary overflow-hidden text-white">
          <div class="row">
            <div class="col-lg-10">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="mailto:info@eternumcolumbarium.com" class="text-white"><i class="fa fa-envelope mr-1"></i> info@eternumcolumbarium.com</a>
                <a href="tel:00447821451233" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +44 (0) 7821 451233</a>
                <a href="tel:00353852148301" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +353 (0) 852 148301</a>
              </p>
            </div>
            <div class="col-lg-2 text-center text-lg-right mob-px-0 d-none d-lg-block">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="https://www.facebook.com/EternumColumbarium/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-facebook ml-4"></i></a>
                <a href="https://www.instagram.com/eternumcolumbarium/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/EternumNILtd" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
        <div class="container-fluid px-5 ipad-px-3 ">
          <div class="row">
            <div class="col-4 col-lg-2 pb-3 pt-2">
              <a href="/">
                <img src="/img/logos/logo.svg" class="menu_logo @if($page =='Homepage Alt') menu_logo_normal @endif" alt="Eternum Columbarium Logo"/>
              </a>
            </div>
            <div class="col-8 col-lg-10 text-right pt-4 mt-2 d-none d-lg-block">
              <div class="menu-links d-inline-block">
                <a href="{{route('welcome')}}" class="menu-item text-dark cursor-pointer">Home</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('about')}}" class="menu-item text-dark cursor-pointer">About</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('partnerships')}}" class="menu-item cursor-pointer">Partnerships</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('locations')}}" class="menu-item cursor-pointer">Locations</a>
              </div>
              {{-- <div class="menu-links d-inline-block ">
                <a href="{{route('memorials')}}" class="menu-item text-dark cursor-pointer">Memorials</a>
              </div> --}}
              <div class="menu-links d-inline-block ">
                <a href="{{route('news')}}" class="menu-item text-dark cursor-pointer">News</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('faqs')}}" class="menu-item text-dark cursor-pointer">FAQs</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('contact')}}" class="menu-item text-dark cursor-pointer">Contact</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      <div id="mobile-menu" class="mobile-menu">
        <div class="container-fluid px-3">
         <div class="row">
          <a href="/">
            <img src="/img/logos/logo.svg" class="menu_logo" width="150" alt="Eternum Columbarium Logo"/>
          </a>
          <div class="col-lg-10 pt-5 mt-5">
            <div class="menu-links d-block pt-4">
              <p class="mb-0"><a href="{{route('welcome')}}" class="menu-item">Home</a></p>
              <p class="mb-0"><a href="{{route('about')}}" class="menu-item">About</a></p>
              <p class="mb-0"><a href="{{route('partnerships')}}" class="menu-item">Partnerships</a></p>
              <p class="mb-0"><a href="{{route('locations')}}" class="menu-item">Locations</a></p>
              {{-- <p class="mb-0"><a href="{{route('memorials')}}" class="menu-item">Memorials</a></p> --}}
              <p class="mb-0"><a href="{{route('news')}}" class="menu-item">News</a></p>
              <p class="mb-0"><a href="{{route('faqs')}}" class="menu-item">Faqs</a></p>
              <p class="mb-0"><a href="{{route('contact')}}" class="menu-item">Contact</a></p>
              <p class="mb-0 text-large mt-3">
                <a href="https://www.facebook.com/EternumColumbarium/" target="_blank"><i class="fa fa-facebook text-primary"></i></a>
                <a href="https://www.instagram.com/eternumcolumbarium/" target="_blank"><i class="fa fa-instagram ml-3 text-primary"></i></a>
                <a href="https://twitter.com/EternumNILtd" target="_blank"><i class="fa fa-twitter ml-3 text-primary"></i></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @yield('header')
    <main id="content">
      <div id="menu-trigger"></div>
      @yield('content')
    </main>
    <footer class="pt-5 pb-0 mob-pt-0 container-fluid text-center text-lg-left position-relative z-2">
      <div class="row py-5 mob-pb-4 mob-mt-0">
        <div class="container pt-4">
          <div class="row">
            <div class="col-lg-3 mb-3 pr-5 mob-px-3">
              <img src="/img/logos/logo-white.svg" alt="Eternum Columbarium logo" class="mb-4 footer-logo" />
              {{-- <p class="mb-0 d-lg-none"><a href="{{route('tandcs')}}">Terms & Conditions</a></p> --}}
              <p class="mb-0 d-lg-none"><a href="/docs/PrivacyAndCookiePolicy3.0.pdf" target="_blank">Privacy Policy</a></p>
            </div>
            <div class="col-lg-5 mb-3 d-none d-lg-block">
              <div class="row pl-5">
                <div class="col-6">
                  <p class="mb-0"><a href="{{route('welcome')}}">Home</a></p>
                  
                  <p class="mb-0"><a href="{{route('about')}}">About</a></p>
                  <p class="mb-0"><a href="{{route('partnerships')}}">Partnerships</a></p>
                  <p class="mb-0"><a href="{{route('locations')}}">Locations</a></p>
                  {{-- <p class="mb-0"><a href="{{route('memorials')}}">Memorials</a></p> --}}
                  <p class="mb-0"><a href="{{route('news')}}">News</a></p>
                </div>
                <div class="col-6">
                  <p class="mb-0"><a href="{{route('faqs')}}">FAQs</a></p>
                  <p class="mb-0"><a href="{{route('contact')}}">Contact</a></p>
                  <p class="mb-0"><a href="/terms-and-conditions-25-03-2021.pdf" target="_blank">Terms & Conditions</a></p>
                  <p class="mb-0"><a href="{{route('privacy-policy')}}">Privacy Policy</a></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 mb-3 text-lg-right">
              <p class="mb-2">
                <a href="https://www.facebook.com"><i class="fa fa-facebook-square ml-2"></i></a>
                <a href="https://www.instagram.com"><i class="fa fa-instagram ml-2"></i></a>
                <a href="https://twitter.com/EternumNILtd"><i class="fa fa-twitter ml-2"></i></a>
              </p>
              <p class="mb-1"><a href="tel:00447821451233" class="text-white">+44 (0) 7821 451233</a></p>
              <p class="mb-1"><a href="tel:00353852148301" class="text-white">+353 (0) 852 148301</a></p>
              <p><a href="mailto:info@eternumcolumbarium.com">info@eternumcolumbarium.com</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-12">
          <p class="text-smaller">&copy;{{Carbon\Carbon::now()->format('Y')}} Eternum (N.I.) Ltd. <span class="d-none d-md-inline">|</span><br class="d-md-none" /> <a href="https://elementseven.co" target="_blank">Website by Element Seven</a></p>
        </div>
      </div>
    </footer>
    @yield('modals')
  </div>
  <div id="menu_body_hide"></div>
  <div id="loader">
    <div class="vert-mid">
      <div class="card p-5">
        <img id="loader-success" src="/img/icons/success.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <img id="loader-error" src="/img/icons/error.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <div id="loader-message"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                  <p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
                </div>
                <div class="col-md-6">
                  @yield('loader-buttons')
                  <a id="loader-link">
                    <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                  </a>
                  <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@yield('prescripts')
<script src="{{ asset('js/app.js') }}"></script>
<script>
	window.addEventListener("load", function(){
   window.cookieconsent.initialise({
     "palette": {
       "popup": {
         "background": "#040819",
         "text": "#ffffff"
       },
       "button": {
         "background": "#b39e7a",
         "text": "#ffffff",
         "border-radius": "30px"
       }
     }
   })});
 </script>
 @yield('scripts')
</body>
</html>