@php
$page = 'News';
$pagetitle = 'Latest News - Eternum Columbarium';
$metadescription = 'See our latest news and advice';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://eternumcolumbarium.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 text-center">
      <h1 class="mt-5 mob-mt-0 faq-title">Latest News</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic" class="my-3" />
    </div>
  </div>
</header>
@endsection
@section('content')
<news-index :categories="{{$categories}}"></news-index>
<footer-top></footer-top>
@endsection