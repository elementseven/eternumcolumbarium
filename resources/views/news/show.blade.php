@php
$page = 'News';
$pagetitle = $post->title . ' | Eternum Columbarium';
$metadescription = $post->excerpt;
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://eternumcolumbarium.com' . $post->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 mob-mb-5 text-center text-lg-left">
    <div class="col-xl-12 pt-5 mob-px-4 mb-5 mob-mb-0">
      <p class="pt-5 mob-pt-0 mb-1"><b><a href="{{route('news')}}"><i class="fa fa-arrow-circle-left"></i>&nbsp; Browse news</a></b></p>
      <h1 class="blog-title mb-4">{{$post->title}}</h1>
      <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic"/>
      <p class="text-larger mt-3
      ">{{$post->excerpt}}</p> 
      <p class="mb-1 text-larger"><b>Share this article:</b></p>
      <p class="text-larger">
          <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
          <i class="fa fa-linkedin ml-2"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
          <i class="fa fa-twitter ml-2"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
          <i class="fa fa-whatsapp ml-2"></i>
        </a>
      </p>
      
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 mob-py-0 text-center text-lg-left">
        <div class="col-xl-10 mob-mt-0 blog-body">
          <img src="{{$post->getFirstMediaUrl('blogs')}}" class="w-100 mb-5" alt="{{$post->name}} blogs image"/>
            
          {!!$post->body!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1 text-larger"><b>Share this article:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<div class="container py-5 mob-px-3">
    <div class="row">
        <div class="col-12 text-center mb-4">
            <h2 class="mb-4 grey-text">More News</h2>
            <img src="/img/shapes/under-title.svg" width="200" alt="Under title graphic"/>
        </div>
        @foreach($others as $key => $post)
        @if($key <= 2)
        <div class="col-md-4 mb-5">
          <a href="{{route('news-single', ['slug' => $post->slug])}}">
            <div class="card border-0 shadow overflow-hidden post-box text-center text-md-left text-dark">
              <div class="post-image">
                <picture> 
                  <source media="(min-width: 900px)" srcset="{{$post->getFirstMediaUrl('blogs', 'normal')}} 1x, {{$post->getFirstMediaUrl('blogs', 'double-webp')}} 2x" type="image/webp"/> 
                  <source media="(min-width: 601px)" srcset="{{$post->getFirstMediaUrl('blogs', 'normal-webp')}} 1x, {{$post->getFirstMediaUrl('blogs', 'double-webp')}} 2x" type="image/webp"/> 
                  <source srcset="{{$post->getFirstMediaUrl('blogs', 'normal-webp')}} 1x, {{$post->getFirstMediaUrl('blogs', 'double-webp')}} 2x" type="image/webp"/> 
                  <img srcset="{{$post->getFirstMediaUrl('blogs', 'normal')}} 600w, {{$post->getFirstMediaUrl('blogs', 'double')}} 900w, {{$post->getFirstMediaUrl('blogs', 'double')}} 1440w" src="{{$post->getFirstMediaUrl('blogs', 'double')}}" type="{{$post->getFirstMedia('blogs')->mime_type}}" alt="{{$post->title}}" class="w-100" />
                </picture>
              </div>
              <div class="p-4">
                <h4 class="post-title">{{$post->title}}</h4>
                <p class="post-exerpt text-small mb-3">{{$post->excerpt}}</p>
                <p class="mb-0"><b>Read more</b> <i class="fa fa-arrow-circle-right ml-1"></i></p>
              </div>
            </div>
          </a>
        </div>
        @endif
        @endforeach
    </div>
</div>
<footer-top></footer-top>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection