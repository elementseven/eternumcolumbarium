<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Pages
Route::get('/', 'PageController@welcome')->name('welcome');
Route::get('/about', 'PageController@about')->name('about');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/privacy-policy', 'PageController@privacyPolicy')->name('privacy-policy');
Route::get('/terms-and-conditions', 'PageController@tandcs')->name('tandcs');

// News routes
Route::get('/news', 'PageController@news')->name('news');
Route::get('/news/get', 'PageController@getPosts')->name('get-posts');
Route::get('/news/{slug}', 'PageController@newsShow')->name('news-single');

// Memorials routes
Route::get('/memorials/get', 'PageController@getMemorials')->name('get-posts');
Route::get('/memorials', 'PageController@memorials')->name('memorials');
Route::get('/memorials/{slug}', 'PageController@memorialsShow')->name('memorials-single');

// FAQs routes
Route::get('/faqs', 'PageController@faqs')->name('faqs');
Route::get('/faqs/get', 'FaqController@get')->name('faqs.get');

// Locations Routes
Route::get('/locations', 'PageController@locations')->name('locations');
Route::get('/locations/get', 'PartnerController@get')->name('get.locations');
Route::get('/locations/{slug}', 'PageController@locationsShow')->name('locations-single');
Route::get('/confirm-location/{partner}', 'PartnerController@confirmLocation')->name('confirm.location');
Route::get('/confirm-lease/{lease}', 'PartnerController@confirmLease')->name('confirm.lease');
Route::get('/positions/get/{partner}', 'PartnerController@getPostions')->name('positions.get');


// Partnerships routes
Route::get('/partnerships', 'PageController@partnerships')->name('partnerships');

// Send enquiry form
Route::post('/send-message', 'SendMail@enquiry')->name('send-message');

// Shop Routes
Route::get('/basket', 'ShopController@basket')->name('basket');
Route::get('/checkout', 'ShopController@checkout')->name('checkout');
// Route::get('/checkout', function(){
// 	return redirect('/emptybasket-contact');
// })->name('checkout');
Route::get('/emptybasket-contact', 'ShopController@emptyBasketContact')->name('emptyBasketContact');
Route::get('/add-to-basket/{position}/{lease}', 'ShopController@addToBasket')->name('addtobasket');
Route::get('/shop/remove/{rowId}', 'ShopController@remove')->name('shop.removefrombasket');
Route::get('/empty-basket', 'ShopController@emptyBasket')->name('emptybasket');

// Checkout Routes
Route::post('/user/store', 'ShopController@createUser')->name('shop.create.user');
Route::get('/user/get-intent', 'ShopController@getIntent')->name('shop.user.getintent');
Route::post('/address/store', 'ShopController@storeAddress')->name('shop.address.store');
Route::post('/payment/store', 'ShopController@payment')->name('shop.payment');
Route::get('/shop/success/{transaction_id}', 'ShopController@success')->name('success');

// Uncomment this to add lots of postions
Route::get('/add-positions/{partner}/{rows}/{cols}', 'PageController@addPositions')->name('add.positions');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', function(){
	Auth::logout();
	return redirect('/login');
})->name('home');

// Employer middlware
Route::group(['middleware' => 'App\Http\Middleware\CustomerMiddleware'], function(){

	Route::get('/customers/dashboard', 'HomeController@dashboard')->name('customer-dashboard');

});
