<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/categories/store', 'CategoryController@store')->name('categories-store');
Route::get('/categories/get/{limit}', 'CategoryController@getCategories')->name('categories-get');
Route::get('/categories/del/{category}', 'CategoryController@destroy')->name('categories-destroy');

Route::get('/news/get', 'PageController@getPosts')->name('get-posts');

Route::post('/news/store', 'PostController@store')->name('news-store');
Route::post('/news/update/{post}', 'PostController@update')->name('news-update');
Route::get('/news/get-posts/{category}/{limit}', 'PostController@getPosts')->name('get-posts');
Route::get('/news/del/{post}', 'PostController@destroy')->name('news-destroy');
Route::post('/news/image-upload', 'PostController@imageUpload')->name('news-image-upload');

Route::get('/analytics/lastweek', 'AnalyticsController@lastweek')->name('analytics-lastweek');
Route::get('/analytics/user-type', 'AnalyticsController@userTypes')->name('analytics-user-types');
Route::get('/analytics/top-referrers', 'AnalyticsController@topReferrers')->name('analytics-top-referrers');
Route::get('/analytics/popular-pages', 'AnalyticsController@popularPages')->name('analytics-popular-pages');